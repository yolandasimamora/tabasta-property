<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Auth\DefaultPasswordHasher;
use Cake\ORM\TableRegistry;


/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */


class TabastaadminController extends AppController
{

    public function initialize() {
        parent::initialize();
        $this->loadModel("Users");
        $this->loadModel("Properties");
        $this->loadModel("Facilities");
        $this->loadModel('Kota');
        $this->loadModel('Provinsi');
        $this->loadModel('PropertyFacilitiesRelations');
        $this->loadModel('PropertyArea');
        $this->loadModel('PropertyCertificates');
        $this->loadModel('PropertyDevelopmentPlans');
        $this->loadModel('PropertyAreaTypes');
        $this->loadModel('PropertyAptPriceByTypes');
        $this->loadModel('PropertyInvestmentInformations');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
        'loginAction' => [
            'controller' => 'Users',
            'action' => 'login'        ],
        'authError' => 'Did you really think you are allowed to see that?',
        'authenticate' => [
            'Form' => [
            'userModel' => 'UserAdmins',
            'fields' => [
                'username' => 'username',
                'password' => 'password',
            ]
        ]
        ],
        'loginRedirect' => [
            'controller' => 'Users',
            'action' => 'login'
        ],
        'logoutRedirect' => [
            'controller' => 'Users',
            'action' => 'login',
        ],
        'storage' => 'Session'
        ]);

        $this->loadModel('Users');
        $auth = $this->request->session()->read('Auth.User');
        if (!empty($auth)){
            $userId = $auth['id'];
            $userinfo = $this->Users->get($userId);
            $this->set(compact('userinfo'));
        }
        $this->viewBuilder()->setLayout('admin');

    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */

    public function login()
    {
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
            }
            $this->Flash->error(__('Invalid username or password, try again'));
        }
    }


    public function logout()
    {
        $this->Flash->success('You are now logged out');
        return $this->redirect($this->Auth->logout());
    }


    public function index()
    {

        $apts = $this->Properties->find('all', [
                'conditions' => [
                    'type_property' => 1
                ]
                
        ])->toArray();
        $apts = count($apts);

        $house = $this->Properties->find('all', [
                'conditions' => [
                    'type_property' => 2
                ]
                
        ])->toArray();
        $house = count($house);
        $this->set(compact('house', 'apts'));

    }

    public function ajaxCityId($id = null, $key = null, $cur = null)
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->loadModel('Kota');
        $this->set('types', $this->Kota->find('all', array('conditions' => array( 'Kota.province_id =' => $id), 'order' => array('Kota.name' => 'asc'))));
        $this->set('key', $key);

        $this->set(compact('types'));
        $this->set('_serialize', ['types']);
    }


    /* Apartments -- Start */
    public function apartments()
    {
        $apts = $this->Properties->find('all', [
                'conditions' => [
                    'type_property' => 1
                ]
                
            ])->toArray();
        $countApts = count($apts);

        //$webdev_category = $this->WebdevCategories->find('all');
        //$portfolio_category = $this->PortfolioCategories->find('all');

        $this->set(compact('apts', 'countApts'));
    }

    public function apartmentView($id = null)
    {

        $apartment = $this->Properties->get($id, [
            'contain' => ['PropertyImageFiles', 'PropertyCertificates', 'PropertyDevelopmentPlans', 'PropertyFacilitiesRelations.Facilities', 'PropertyArea', 'PropertyAptPriceByTypes', 'PropertyInvestmentInformations']
        ]);        

        $this->loadModel('Provinsi');
        $provinsi = $this->Provinsi->find()
        ->where([
        'Provinsi.id' => $apartment->provinsi_id
        ]);
        
        $this->loadModel('Kota');
        $kota = $this->Kota->find()
        ->where([
        'Kota.id' => $apartment->kota_id
        ]);

        $this->set(compact('apartment', 'provinsi', 'kota'));
    }

    public function addApt()
    {
        $this->set(
            'provinsis', 
        $this->Provinsi->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ]));

        $kota = $this->Kota->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ])->contain(['Provinsi']);

        $this->set(
            'facilities', 
        $this->Facilities->find('all'));

        $apartment = $this->Properties->newEntity();
        if ($this->request->is('post')) {
            $apartment = $this->Properties->patchEntity($apartment, $this->request->getData());
            if ($this->Properties->save($apartment)) {

                //save certificates
                if(!empty($this->request->data['certificate_name']) && $this->request->data['certificate_name'][0] != '')
                {
                    $certis = $this->request->data['certificate_name'];
                    foreach($certis as $certi)
                    {
                        $certificate = $this->PropertyCertificates->newEntity([
                            'property_id'           => $apartment->id,
                            'certificates_name'     => $certi,
                            'created'               => date("Y-m-d H:i:s"),
                            'modified'              => date("Y-m-d H:i:s"),
                        ]);
                        $saveCerti = $this->PropertyCertificates->save($certificate);
                    }
                }  

                //save development plan
                if(!empty($this->request->data['dev_plan']) && $this->request->data['dev_plan'][0] != '')
                {
                    $devs = $this->request->data['dev_plan'];
                    $status = $this->request->data['status'];
                    $time = strtotime($this->request->data['time']);
                    $newformat = date('Y-m-d',$time); 
                    $this->request->data['time'] = $newformat;
                    $dev_plan = $this->PropertyDevelopmentPlans->newEntity([
                        'property_id'           => $apartment->id,
                        'dev_plan'              => $devs,
                        'status'                => $status,
                        'time'                  => $newformat,
                        'created'               => date("Y-m-d H:i:s"),
                        'modified'              => date("Y-m-d H:i:s"),
                    ]);
                    $saveDevPlan = $this->PropertyDevelopmentPlans->save($dev_plan);
                }  

                // //save investment information
                // if(!empty($this->request->data['apt_unit']||$this->request->data['area']||$this->request->data['avg_selling_price']||$this->request->data['selling_price_per_m2']||$this->request->data['avg_rental_price_monthly']||$this->request->data['avg_rental_price_annually']||$this->request->data['avg_rental_price_annually_percentage']||$this->request->data['service_charge_annually']||$this->request->data['agent_fee_annually']||$this->request->data['expected_annual_return'])){

                //     $apt_unit = $this->request->data['apt_unit'];
                //     $area = $this->request->data['area'];
                //     $avg_selling_price = $this->request->data['avg_selling_price'];
                //     $selling_price_per_m2 = $this->request->data['selling_price_per_m2']; 
                //     $avg_rental_price_monthly = $this->request->data['avg_rental_price_monthly']; 
                //     $avg_rental_price_annually = $this->request->data['avg_rental_price_annually']; 
                //     $avg_rental_price_annually_percentage = $this->request->data['avg_rental_price_annually_percentage'];
                //     $service_charge_annually = $this->request->data['service_charge_annually']; 
                //     $agent_fee_annually = $this->request->data['agent_fee_annually']; 
                //     $expected_annual_return = $this->request->data['expected_annual_return']; 
                //     $investment_info = $this->PropertyInvestmentInformations->newEntity([
                //         'property_id'                           => $apartment->id,
                //         'apt_unit'                              => $apt_unit,
                //         'area'                                  => $area,
                //         'avg_selling_price'                     => $avg_selling_price,
                //         'selling_price_per_m2'                  => $selling_price_per_m2,
                //         'avg_rental_price_monthly'              => $avg_rental_price_monthly,
                //         'avg_rental_price_annually'             => $avg_rental_price_annually,
                //         'avg_rental_price_annually_percentage'  => $avg_rental_price_annually_percentage,
                //         'service_charge_annually'               => $service_charge_annually,
                //         'agent_fee_annually'                    => $agent_fee_annually,
                //         'expected_annual_return'                => $expected_annual_return,
                //         'created'                               => date("Y-m-d H:i:s"),
                //         'modified'                              => date("Y-m-d H:i:s"),
                //     ]);
                //     $saveInvestmentInformation = $this->PropertyInvestmentInformations->save($investment_info);
                // }

                //save price by room types
                if(!empty($this->request->data['studio']||$this->request->data['rent_studio']||$this->request->data['bedroom_1']||$this->request->data['rent_bedroom_1']||$this->request->data['bedroom_2']||$this->request->data['rent_bedroom_2']||$this->request->data['penthouse']||$this->request->data['rent_penthouse'])){

                    $studio = $this->request->data['studio'];
                    $rent_studio = $this->request->data['rent_studio'];
                    $bedroom_1 = $this->request->data['bedroom_1'];
                    $rent_bedroom_1 = $this->request->data['rent_bedroom_1'];
                    $bedroom_2 = $this->request->data['bedroom_2'];
                    $rent_bedroom_2 = $this->request->data['rent_bedroom_2'];
                    $penthouse = $this->request->data['penthouse'];
                    $rent_penthouse = $this->request->data['rent_penthouse'];

                    $price_by_type = $this->PropertyAptPriceByTypes->newEntity([
                        'property_id'           => $apartment->id,
                        'studio'                => $studio,
                        'rent_studio'           => $rent_studio,
                        'bedroom_1'             => $bedroom_1,
                        'rent_bedroom_1'        => $rent_bedroom_1,
                        'bedroom_2'             => $bedroom_2,
                        'rent_bedroom_2'        => $rent_bedroom_2,
                        'penthouse'             => $penthouse,
                        'rent_penthouse'        => $rent_penthouse,
                        'created'               => date("Y-m-d H:i:s"),
                        'modified'              => date("Y-m-d H:i:s"),
                    ]);
                    $savePrice = $this->PropertyAptPriceByTypes->save($price_by_type);
                }

                //save image
                $this->loadModel('PropertyImageFiles');
                if (!empty($this->request->data['logo']['name'])) {
                    $ext = substr(strtolower(strrchr($this->request->data['logo']['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'png'); //set allowed extensions
                    $setNewFileName = time() . "_" . rand(000000, 999999);
                    //only process if the extension is valid
                    $uniquecode = substr(md5(microtime()),0,10); //generate random string
                    $randomKey = substr(md5(microtime()),0,10);
                    $this->request->data['activation_key'] = $uniquecode;
                    if (!empty($ext)) {
                        if (in_array($ext, $arr_ext)) {
                            $image = $this->PropertyImageFiles->newEntity([
                                'property_id'     => $apartment->id,
                                'file_name'       => $setNewFileName . '.' . $ext,'activation_key'  => $uniquecode,
                                'file_path'       => 'image_file/',
                                'created'         => date("Y-m-d H:i:s"),
                                'modified'        => date("Y-m-d H:i:s"),
                            ]);
                            //do the actual uploading of the file. First arg is the tmp name, second arg is 
                            //where we are putting it
                            move_uploaded_file($this->request->data['logo']['tmp_name'], WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext);
                            $imagine = new \Imagick(WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext);

                            // 写真サイズ取得
                            $img_width  = $imagine->getImageWidth();
                            $img_height = $imagine->getImageHeight();
                                     
                            // 写真リサイズ
                            // 縦サイズ601px以上だったら縦サイズ600pxにリサイズ
                            $max_height = 150;
                            $max_width = 150;
                            if (!empty($img_width) && !empty($img_height) && ($max_height < $img_height) && ($max_width < $img_width)) {
                            // リサイズ処理
                                $imagine->adaptiveResizeImage(150, 150, true);
                            }
                                     
                            // 写真上書き保存
                            if (!$imagine->writeImage(WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext)) {
                            // 上書き保存エラー
                            }
                                     
                            // Imagick clear
                            $imagine->clear();

                            //prepare the filename for database entry 
                            $imageFileName = $setNewFileName . '.' . $ext;
                            $saveImage = $this->PropertyImageFiles->save($image);
                            } else {
                                $this->Flash->error(__('Wrong file type'));
                            }
                    }
                }
                if (!empty($this->request->data['file_name'])) {
                    foreach($this->request->data['file_name'] as $img) {
                    $ext = substr(strtolower(strrchr($img['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'png'); //set allowed extensions
                    $setNewFileName = time() . "_" . rand(000000, 999999);
                    //only process if the extension is valid
                    $uniquecode = substr(md5(microtime()),0,10); //generate random string
                    $randomKey = substr(md5(microtime()),0,10);
                    $this->request->data['activation_key'] = $uniquecode;
                    if (!empty($ext)) {
                        if (in_array($ext, $arr_ext)) {
                            $image = $this->PropertyImageFiles->newEntity([
                                'property_id'     => $apartment->id,
                                'file_name'       => $setNewFileName . '.' . $ext,'activation_key'  => $uniquecode,
                                'file_path'       => 'image_file/',
                                'created'         => date("Y-m-d H:i:s"),
                                'modified'        => date("Y-m-d H:i:s"),
                            ]);
                            //do the actual uploading of the file. First arg is the tmp name, second arg is 
                            //where we are putting it
                            move_uploaded_file($img['tmp_name'], WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext);
                            $imagine = new \Imagick(WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext);

                            // 写真サイズ取得
                            $img_width  = $imagine->getImageWidth();
                            $img_height = $imagine->getImageHeight();
                                     
                            // 写真リサイズ
                            // 縦サイズ601px以上だったら縦サイズ600pxにリサイズ
                            $max_height = 150;
                            $max_width = 150;
                            if (!empty($img_width) && !empty($img_height) && ($max_height < $img_height) && ($max_width < $img_width)) {
                            // リサイズ処理
                                $imagine->adaptiveResizeImage(150, 150, true);
                            }
                                     
                            // 写真上書き保存
                            if (!$imagine->writeImage(WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext)) {
                            // 上書き保存エラー
                            }
                                     
                            // Imagick clear
                            $imagine->clear();

                            //prepare the filename for database entry 
                            $imageFileName = $setNewFileName . '.' . $ext;
                            $saveImage = $this->PropertyImageFiles->save($image);
                            } else {
                                $this->Flash->error(__('Wrong file type'));
                            }
                        }
                    }
                } 

                //save facilities
                if(!empty($this->request->data['facilities_id']))
                {
                    $facilities = $this->request->data['facilities_id'];
                    foreach($facilities as $facility)
                    {
                        $facs = $this->PropertyFacilitiesRelations->newEntity([
                            'property_id'       => $apartment->id,
                            'facilities_id'     => $facility,
                            'created'         => date("Y-m-d H:i:s"),
                            'modified'        => date("Y-m-d H:i:s"),
                        ]);
                        $saveFacs = $this->PropertyFacilitiesRelations->save($facs);
                    }
                }  

                //save area information
                if(!empty($this->request->data['public_transportation']) || !empty($this->request->data['neighbourhood']) || !empty($this->request->data['industrial_area']) || !empty($this->request->data['supermarket']))
                {
                    $area = $this->PropertyArea->newEntity([
                            'property_id'               => $apartment->id,
                            'public_transportation'     => $this->request->data['public_transportation'],
                            'neighbourhood'             => $this->request->data['neighbourhood'],
                            'industrial_area'           => $this->request->data['industrial_area'],
                            'supermarket'               => $this->request->data['supermarket'],
                            'created'                   => date("Y-m-d H:i:s"),
                            'modified'                  => date("Y-m-d H:i:s")
                    ]);
                    $savarea = $this->PropertyArea->save($area);   
                }

                $this->Flash->success(__('The apartment data has been saved.'));
                return $this->redirect(['action' => 'apartments']);
            }
            $this->Flash->error(__('The apartment could not be saved. Please, try again.'));
        }
        $this->set(compact('apartments', 'provinsis', 'kota', 'facilities'));
    }

    public function editApt($id = null)
    {
        $this->set(
            'provinsis', 
        $this->Provinsi->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ]));

        $this->set(
            'kota', 
        $this->Kota->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ]));

        $facilities = $this->Facilities->find("list")->toArray();

        $selectedCats = $this->PropertyFacilitiesRelations->find('all', [
            'conditions' => [
                'property_id' => $id
            ]
        ])->toArray();


        if (empty($selectedCats)){
            $selectedCat = array(0);
        } else {
            foreach ($selectedCats as $i){
                $selectedCat[] = $i->facilities_id;
            }
        }

        $apartments = $this->Properties->find('all', [
            'contain' => ['PropertyImageFiles', 'PropertyCertificates', 'PropertyDevelopmentPlans', 'PropertyArea', 'PropertyAptPriceByTypes', 'PropertyInvestmentInformations.Properties'],
            'conditions' => [
                'id'  => $id
            ]
        ])->first();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $apartment = $this->Properties->patchEntity($apartments, $this->request->getData());
            if ($this->Properties->save($apartment)) {


                //save certificates
                if(!empty($this->request->data['certificate_name']) && $this->request->data['certificate_name'][0] != '')
                {
                    $certis = $this->request->data['certificate_name'];
                    foreach($certis as $certi)
                    {
                        $certificate = $this->PropertyCertificates->newEntity([
                            'property_id'           => $apartment->id,
                            'certificates_name'     => $certi,
                            'created'               => date("Y-m-d H:i:s"),
                            'modified'              => date("Y-m-d H:i:s"),
                        ]);
                        $saveCerti = $this->PropertyCertificates->save($certificate);
                    }
                }  

                //save development plan
                if(!empty($this->request->data['dev_plan']) && $this->request->data['dev_plan'][0] != '')
                {
                    $devs = $this->request->data['dev_plan'];
                    $status = $this->request->data['status'];
                    $time = strtotime($this->request->data['time']);
                    $newformat = date('Y-m-d',$time); 
                    $this->request->data['time'] = $newformat;
                    $dev_plan = $this->PropertyDevelopmentPlans->newEntity([
                        'property_id'           => $apartment->id,
                        'dev_plan'              => $devs,
                        'status'                => $status,
                        'time'                  => $newformat,
                        'created'               => date("Y-m-d H:i:s"),
                        'modified'              => date("Y-m-d H:i:s"),
                    ]);
                    $saveDevPlan = $this->PropertyDevelopmentPlans->save($dev_plan);
                }


                //save price info by room type
                if(!empty($this->request->data['studio']||$this->request->data['rent_studio']||$this->request->data['bedroom_1']||$this->request->data['rent_bedroom_1']||$this->request->data['bedroom_2']||$this->request->data['rent_bedroom_2']||$this->request->data['penthouse']||$this->request->data['rent_penthouse'])){

                    $studio = $this->request->data['studio'];
                    $rent_studio = $this->request->data['rent_studio'];
                    $bedroom_1 = $this->request->data['bedroom_1'];
                    $rent_bedroom_1 = $this->request->data['rent_bedroom_1'];
                    $bedroom_2 = $this->request->data['bedroom_2'];
                    $rent_bedroom_2 = $this->request->data['rent_bedroom_2'];
                    $penthouse = $this->request->data['penthouse'];
                    $rent_penthouse = $this->request->data['rent_penthouse'];


                    $prices = TableRegistry::get("PropertyAptPriceByTypes");
                    $conditions = array('property_id'=>$apartment->id);
                    $fields = array('studio'                => $studio,
                                'rent_studio'           => $rent_studio,
                                'bedroom_1'             => $bedroom_1,
                                'rent_bedroom_1'        => $rent_bedroom_1,
                                'bedroom_2'             => $bedroom_2,
                                'rent_bedroom_2'        => $rent_bedroom_2,
                                'penthouse'             => $penthouse,
                                'rent_penthouse'        => $rent_penthouse,
                                'created'               => date("Y-m-d H:i:s"),
                                'modified'              => date("Y-m-d H:i:s"), );
                    $prices->updateAll($fields, $conditions);
                }


                $prices = TableRegistry::get("PropertyAptPriceByTypes");
                $conditions = array('property_id'=>$apartment->id);
                $fields = array('studio'                => $studio,
                            'rent_studio'           => $rent_studio,
                            'bedroom_1'             => $bedroom_1,
                            'rent_bedroom_1'        => $rent_bedroom_1,
                            'bedroom_2'             => $bedroom_2,
                            'rent_bedroom_2'        => $rent_bedroom_2,
                            'penthouse'             => $penthouse,
                            'rent_penthouse'        => $rent_penthouse,
                            'created'               => date("Y-m-d H:i:s"),
                            'modified'              => date("Y-m-d H:i:s"), );
                $prices->updateAll($fields, $conditions);


                //save image
                $this->loadModel('PropertyImageFiles');
                if (!empty($this->request->data['logo']['name'])) {
                    $ext = substr(strtolower(strrchr($this->request->data['logo']['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'png'); //set allowed extensions
                    $setNewFileName = time() . "_" . rand(000000, 999999);
                    //only process if the extension is valid
                    $uniquecode = substr(md5(microtime()),0,10); //generate random string
                    $randomKey = substr(md5(microtime()),0,10);
                    $this->request->data['activation_key'] = $uniquecode;
                    if (!empty($ext)) {
                        if (in_array($ext, $arr_ext)) {
                            $image = $this->PropertyImageFiles->newEntity([
                                'property_id'     => $apartment->id,
                                'file_name'       => $setNewFileName . '.' . $ext,'activation_key'  => $uniquecode,
                                'file_path'       => 'image_file/',
                                'created'         => date("Y-m-d H:i:s"),
                                'modified'        => date("Y-m-d H:i:s"),
                            ]);
                            //do the actual uploading of the file. First arg is the tmp name, second arg is 
                            //where we are putting it
                            move_uploaded_file($this->request->data['logo']['tmp_name'], WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext);
                            $imagine = new \Imagick(WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext);

                            // 写真サイズ取得
                            $img_width  = $imagine->getImageWidth();
                            $img_height = $imagine->getImageHeight();
                                     
                            // 写真リサイズ
                            // 縦サイズ601px以上だったら縦サイズ600pxにリサイズ
                            $max_height = 150;
                            $max_width = 150;
                            if (!empty($img_width) && !empty($img_height) && ($max_height < $img_height) && ($max_width < $img_width)) {
                            // リサイズ処理
                                $imagine->adaptiveResizeImage(150, 150, true);
                            }
                                     
                            // 写真上書き保存
                            if (!$imagine->writeImage(WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext)) {
                            // 上書き保存エラー
                            }
                                     
                            // Imagick clear
                            $imagine->clear();

                            //prepare the filename for database entry 
                            $imageFileName = $setNewFileName . '.' . $ext;
                            $saveImage = $this->PropertyImageFiles->save($image);
                            } else {
                                $this->Flash->error(__('Wrong file type'));
                            }
                    }
                }
                if (!empty($this->request->data['file_name'])) {
                    foreach($this->request->data['file_name'] as $img) {
                    $ext = substr(strtolower(strrchr($img['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'png'); //set allowed extensions
                    $setNewFileName = time() . "_" . rand(000000, 999999);
                    //only process if the extension is valid
                    $uniquecode = substr(md5(microtime()),0,10); //generate random string
                    $randomKey = substr(md5(microtime()),0,10);
                    $this->request->data['activation_key'] = $uniquecode;
                    if (!empty($ext)) {
                        if (in_array($ext, $arr_ext)) {
                            $image = $this->PropertyImageFiles->newEntity([
                                'property_id'     => $apartment->id,
                                'file_name'       => $setNewFileName . '.' . $ext,'activation_key'  => $uniquecode,
                                'file_path'       => 'image_file/',
                                'created'         => date("Y-m-d H:i:s"),
                                'modified'        => date("Y-m-d H:i:s"),
                            ]);
                            //do the actual uploading of the file. First arg is the tmp name, second arg is 
                            //where we are putting it
                            move_uploaded_file($img['tmp_name'], WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext);
                            $imagine = new \Imagick(WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext);

                            // 写真サイズ取得
                            $img_width  = $imagine->getImageWidth();
                            $img_height = $imagine->getImageHeight();
                                     
                            // 写真リサイズ
                            // 縦サイズ601px以上だったら縦サイズ600pxにリサイズ
                            $max_height = 150;
                            $max_width = 150;
                            if (!empty($img_width) && !empty($img_height) && ($max_height < $img_height) && ($max_width < $img_width)) {
                            // リサイズ処理
                                $imagine->adaptiveResizeImage(150, 150, true);
                            }
                                     
                            // 写真上書き保存
                            if (!$imagine->writeImage(WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext)) {
                            // 上書き保存エラー
                            }
                                     
                            // Imagick clear
                            $imagine->clear();

                            //prepare the filename for database entry 
                            $imageFileName = $setNewFileName . '.' . $ext;
                            $saveImage = $this->PropertyImageFiles->save($image);
                            } else {
                                $this->Flash->error(__('Wrong file type'));
                            }
                        }
                    }
                }   

                //save facilities
                if (isset($this->request->data['facility'])) {
                    $facility = $this->request->data['facility'];
                    if(count($selectedCat) == count($facility)){
                        foreach($facility as $facs){
                            $facs = $this->PropertyFacilitiesRelations->newEntity([
                                'property_id'       => $apartments->id,
                                'facilities_id'     => $facs,
                                'created'         => date("Y-m-d H:i:s"),
                                'modified'        => date("Y-m-d H:i:s"),
                            ]);
                        $saveFacs = $this->PropertyFacilitiesRelations->save($facs);
                    }
                } else {
                    $query = $this->PropertyFacilitiesRelations->query();
                    $query->delete()
                    ->where(['property_id' => $id])
                    ->execute();
                    foreach($facility as $facs){
                    $facs = $this->PropertyFacilitiesRelations->newEntity([
                        'property_id'       => $apartments->id,
                        'facilities_id'     => $facs,
                        'created'         => date("Y-m-d H:i:s"),
                        'modified'        => date("Y-m-d H:i:s"),
                    ]);
                    $saveType = $this->PropertyFacilitiesRelations->save($facs);
                    }
                }
                } else {
                    $query = $this->PropertyFacilitiesRelations->query();
                    $query->delete()
                     ->where(['property_id' => $id])
                    ->execute();
                }

                //save area information
                if(!empty($this->request->data['public_transportation']) || !empty($this->request->data['neighbourhood']) || !empty($this->request->data['industrial_area']) || !empty($this->request->data['supermarket']))
                {
                    $property_area_id = $apartments->property_area[0]->id;
                    $area = $this->PropertyArea->get($property_area_id);
                    $areas = $this->PropertyArea->patchEntity($area, $this->request->data);
                    $savarea = $this->PropertyArea->save($areas);   
                }

                $this->Flash->success(__('The apartment has been saved.'));

                return $this->redirect(['action' => 'apartments']);
            }
            $this->Flash->error(__('The apartment could not be saved. Please, try again.'));
        }
        $this->set(compact('apartments', 'provinsis', 'kota', 'facilities', 'selectedCat', 'selectedCats'));
    }

    public function deleteApt($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $apartments = $this->Properties->get($id);
        if ($this->Properties->delete($apartments)) {
            $this->Flash->success(__('The apartment has been deleted.'));
        } else {
            $this->Flash->error(__('The apartment could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'apartments']);
    }

    public function deleteImageApt($property_id = null, $id = null)
    {
        $this->loadModel('PropertyImageFiles');
        // set default class & message for setFlash
        $class = 'flash_bad';
        $msg   = 'Invalid List Id';
        
        // check id is valid
        if($id!=null && is_numeric($id)) {
            // get the Item
            $item = $this->PropertyImageFiles->find('all', [
            'conditions' => [
                'id'  => $id
            ]
            ])->first();
            
            // check Item is valid
            if(!empty($item)) {
                // try deleting the item
                $item = $this->PropertyImageFiles->get($id);
                if($this->PropertyImageFiles->delete($item)) {
                    $class = 'flash_good';
                    $msg   = 'Your picture was successfully deleted';
                    $this->Flash->success($msg,'default',array('class'=>$class));
                    return $this->redirect(['action'=>'editApt', $property_id]);
                } else {
                    $msg = 'There was a problem deleting your picture, please try again';
                    $this->Flash->error($msg,'default',array('class'=>$class));
                    return $this->redirect(['action'=>'editApt', $property_id]);
                }
            }
        }    
    }

    public function addInvestmentInformation($id = null)
    {
        $investments = $this->PropertyInvestmentInformations->newEntity();
        if ($this->request->is('post')) {
            $apt_unit = $this->request->data['apt_unit'];
            $area = $this->request->data['area'];
            $avg_selling_price = $this->request->data['avg_selling_price'];
            $selling_price_per_m2 = $this->request->data['selling_price_per_m2']; 
            $avg_rental_price_monthly = $this->request->data['avg_rental_price_monthly']; 
            $avg_rental_price_annually = $this->request->data['avg_rental_price_annually']; 
            $avg_rental_price_annually_percentage = $this->request->data['avg_rental_price_annually_percentage'];
            $service_charge_annually = $this->request->data['service_charge_annually']; 
            $agent_fee_annually = $this->request->data['agent_fee_annually']; 
            $expected_annual_return = $this->request->data['expected_annual_return']; 
            $investment_info = $this->PropertyInvestmentInformations->newEntity([
                'property_id'                           => $id,
                'apt_unit'                              => $apt_unit,
                'area'                                  => $area,
                'avg_selling_price'                     => $avg_selling_price,
                'selling_price_per_m2'                  => $selling_price_per_m2,
                'avg_rental_price_monthly'              => $avg_rental_price_monthly,
                'avg_rental_price_annually'             => $avg_rental_price_annually,
                'avg_rental_price_annually_percentage'  => $avg_rental_price_annually_percentage,
                'service_charge_annually'               => $service_charge_annually,
                'agent_fee_annually'                    => $agent_fee_annually,
                'expected_annual_return'                => $expected_annual_return,
                'created'                               => date("Y-m-d H:i:s"),
                'modified'                              => date("Y-m-d H:i:s"),
                ]);

            if ($saveInvestmentInformation = $this->PropertyInvestmentInformations->save($investment_info)) {
                $this->Flash->success(__('The investment information has been saved.'));
                return $this->redirect(['action' => 'apartments']);
            }
            $this->Flash->error(__('The investment information could not be saved. Please, try again.'));
        }
        $this->set(compact('investments'));
    }

    public function editInvestmentInformation($id = null)
    {
        $info = $this->PropertyInvestmentInformations->find('all', [
            'contain' => [],
            'conditions' => [
                'id'  => $id
            ]
        ])->first();

        if ($this->request->is(['patch', 'post', 'put'])) {
            $info = $this->PropertyInvestmentInformations->patchEntity($info, $this->request->getData());
            if ($this->PropertyInvestmentInformations->save($info)) {
                $this->Flash->success(__('The investment information has been saved.'));
                return $this->redirect(['action' => 'edit_apt'.'/'.$info->property_id]);
            }
            $this->Flash->error(__('The investment information could not be saved. Please, try again.'));
        }
        $this->set(compact('info'));
    }

    public function viewInvestmentInformation($id = null)
    {

        $info = $this->PropertyInvestmentInformations->get($id, [
            'contain' => ['Properties']
        ]);        

        $this->set(compact('info'));
    }

    /* Apartments -- End */

    /* Houses -- Start */
    public function houses()
    { 
        $houses = $this->Properties->find('all', [
                'conditions' => [
                    'type_property' => 2
                ]
                
            ])->toArray();
        $countHouses = count($houses);

        //$webdev_category = $this->WebdevCategories->find('all');
        //$portfolio_category = $this->PortfolioCategories->find('all');

        $this->set(compact('houses', 'countHouses'));
    }


    public function houseView($id = null)
    {

        $house = $this->Properties->get($id, [
            'contain' => ['PropertyImageFiles', 'PropertyArea', 'PropertyCertificates', 'PropertyDevelopmentPlans', 'PropertyFacilitiesRelations.Facilities']
        ]);        

        $this->loadModel('Provinsi');
        $provinsi = $this->Provinsi->find()
        ->where([
        'Provinsi.id' => $house->provinsi_id
        ]);
        
        $this->loadModel('Kota');
        $kota = $this->Kota->find()
        ->where([
        'Kota.id' => $house->kota_id
        ]);

        $this->set(compact('house', 'provinsi', 'kota'));
    }


    public function addHouse()
    {

        $this->set(
            'provinsis', 
        $this->Provinsi->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ]));

        $kota = $this->Kota->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ])->contain(['Provinsi']);

        $this->set(
            'facilities', 
        $this->Facilities->find('all'));

        $house = $this->Properties->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['type_property'] = 2;
            $house = $this->Properties->patchEntity($house, $this->request->getData());
            if ($this->Properties->save($house)) {


                //save certificates
                if(!empty($this->request->data['certificate_name']) && $this->request->data['certificate_name'][0] != '')
                {
                    $certis = $this->request->data['certificate_name'];
                    foreach($certis as $certi)
                    {
                        $certificate = $this->PropertyCertificates->newEntity([
                            'property_id'           => $house->id,
                            'certificates_name'     => $certi,
                            'created'               => date("Y-m-d H:i:s"),
                            'modified'              => date("Y-m-d H:i:s"),
                        ]);
                        $saveCerti = $this->PropertyCertificates->save($certificate);
                    }
                }  

                //save development plan
                if(!empty($this->request->data['dev_plan']) && $this->request->data['dev_plan'][0] != '')
                {
                    $devs = $this->request->data['dev_plan'];
                    foreach($devs as $dev)
                    {
                        $dev_plan = $this->PropertyDevelopmentPlans->newEntity([
                            'property_id'           => $house->id,
                            'dev_plan'              => $dev,
                            'created'               => date("Y-m-d H:i:s"),
                            'modified'              => date("Y-m-d H:i:s"),
                        ]);
                        $saveDevPlan = $this->PropertyDevelopmentPlans->save($dev_plan);
                    }
                }  

                //save image
                $this->loadModel('PropertyImageFiles');
                if (!empty($this->request->data['logo']['name'])) {
                    $ext = substr(strtolower(strrchr($this->request->data['logo']['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'png'); //set allowed extensions
                    $setNewFileName = time() . "_" . rand(000000, 999999);
                    //only process if the extension is valid
                    $uniquecode = substr(md5(microtime()),0,10); //generate random string
                    $randomKey = substr(md5(microtime()),0,10);
                    $this->request->data['activation_key'] = $uniquecode;
                    if (!empty($ext)) {
                        if (in_array($ext, $arr_ext)) {
                            $image = $this->PropertyImageFiles->newEntity([
                                'property_id'     => $house->id,
                                'file_name'       => $setNewFileName . '.' . $ext,'activation_key'  => $uniquecode,
                                'file_path'       => 'image_file/',
                                'created'         => date("Y-m-d H:i:s"),
                                'modified'        => date("Y-m-d H:i:s"),
                            ]);
                            //do the actual uploading of the file. First arg is the tmp name, second arg is 
                            //where we are putting it
                            move_uploaded_file($this->request->data['logo']['tmp_name'], WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext);
                            $imagine = new \Imagick(WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext);

                            // 写真サイズ取得
                            $img_width  = $imagine->getImageWidth();
                            $img_height = $imagine->getImageHeight();
                                     
                            // 写真リサイズ
                            // 縦サイズ601px以上だったら縦サイズ600pxにリサイズ
                            $max_height = 150;
                            $max_width = 150;
                            if (!empty($img_width) && !empty($img_height) && ($max_height < $img_height) && ($max_width < $img_width)) {
                            // リサイズ処理
                                $imagine->adaptiveResizeImage(150, 150, true);
                            }
                                     
                            // 写真上書き保存
                            if (!$imagine->writeImage(WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext)) {
                            // 上書き保存エラー
                            }
                                     
                            // Imagick clear
                            $imagine->clear();

                            //prepare the filename for database entry 
                            $imageFileName = $setNewFileName . '.' . $ext;
                            $saveImage = $this->PropertyImageFiles->save($image);
                            } else {
                                $this->Flash->error(__('Wrong file type'));
                            }
                    }
                }
                if (!empty($this->request->data['file_name'])) {
                    foreach($this->request->data['file_name'] as $img) {
                    $ext = substr(strtolower(strrchr($img['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'png'); //set allowed extensions
                    $setNewFileName = time() . "_" . rand(000000, 999999);
                    //only process if the extension is valid
                    $uniquecode = substr(md5(microtime()),0,10); //generate random string
                    $randomKey = substr(md5(microtime()),0,10);
                    $this->request->data['activation_key'] = $uniquecode;
                    if (!empty($ext)) {
                        if (in_array($ext, $arr_ext)) {
                            $image = $this->PropertyImageFiles->newEntity([
                                'property_id'     => $apartment->id,
                                'file_name'       => $setNewFileName . '.' . $ext,'activation_key'  => $uniquecode,
                                'file_path'       => 'image_file/',
                                'created'         => date("Y-m-d H:i:s"),
                                'modified'        => date("Y-m-d H:i:s"),
                            ]);
                            //do the actual uploading of the file. First arg is the tmp name, second arg is 
                            //where we are putting it
                            move_uploaded_file($img['tmp_name'], WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext);
                            $imagine = new \Imagick(WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext);

                            // 写真サイズ取得
                            $img_width  = $imagine->getImageWidth();
                            $img_height = $imagine->getImageHeight();
                                     
                            // 写真リサイズ
                            // 縦サイズ601px以上だったら縦サイズ600pxにリサイズ
                            $max_height = 150;
                            $max_width = 150;
                            if (!empty($img_width) && !empty($img_height) && ($max_height < $img_height) && ($max_width < $img_width)) {
                            // リサイズ処理
                                $imagine->adaptiveResizeImage(150, 150, true);
                            }
                                     
                            // 写真上書き保存
                            if (!$imagine->writeImage(WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext)) {
                            // 上書き保存エラー
                            }
                                     
                            // Imagick clear
                            $imagine->clear();

                            //prepare the filename for database entry 
                            $imageFileName = $setNewFileName . '.' . $ext;
                            $saveImage = $this->PropertyImageFiles->save($image);
                            } else {
                                $this->Flash->error(__('Wrong file type'));
                            }
                        }
                    }
                } 


                //save facilities
                if(!empty($this->request->data['facilities_id']))
                {
                    $facilities = $this->request->data['facilities_id'];
                    foreach($facilities as $facility)
                    {
                        $facs = $this->PropertyFacilitiesRelations->newEntity([
                            'property_id'       => $house->id,
                            'facilities_id'     => $facility,
                            'created'         => date("Y-m-d H:i:s"),
                            'modified'        => date("Y-m-d H:i:s"),
                        ]);
                        $saveFacs = $this->PropertyFacilitiesRelations->save($facs);
                    }
                }  

                //save area information
                if(!empty($this->request->data['public_transportation']) || !empty($this->request->data['neighbourhood']) || !empty($this->request->data['industrial_area']) || !empty($this->request->data['supermarket']))
                {
                    $area = $this->PropertyArea->newEntity([
                            'property_id'               => $house->id,
                            'public_transportation'     => $this->request->data['public_transportation'],
                            'neighbourhood'             => $this->request->data['neighbourhood'],
                            'industrial_area'           => $this->request->data['industrial_area'],
                            'supermarket'               => $this->request->data['supermarket'],
                            'created'                   => date("Y-m-d H:i:s"),
                            'modified'                  => date("Y-m-d H:i:s")
                    ]);
                    $savarea = $this->PropertyArea->save($area);   
                }

                $this->Flash->success(__('The house data has been saved.'));
                return $this->redirect(['action' => 'houses']);
            }
            $this->Flash->error(__('The house could not be saved. Please, try again.'));
        }
        $this->set(compact('house', 'provinsis', 'kota', 'facilities'));
    }

    public function editHouse($id = null)
    {
        $this->set(
            'provinsis', 
        $this->Provinsi->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ]));

        $this->set(
            'kota', 
        $this->Kota->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ]));

        $facilities = $this->Facilities->find("list")->toArray();

        $selectedCats = $this->PropertyFacilitiesRelations->find('all', [
            'conditions' => [
                'property_id' => $id
            ]
        ])->toArray();


        if (empty($selectedCats)){
            $selectedCat = array(0);
        } else {
            foreach ($selectedCats as $i){
                $selectedCat[] = $i->facilities_id;
            }
        }

        $houses = $this->Properties->find('all', [
            'contain' => ['PropertyImageFiles', 'PropertyArea', 'PropertyCertificates', 'PropertyDevelopmentPlans'],
            'conditions' => [
                'id'  => $id
            ]
        ])->first();
        if ($this->request->is(['patch', 'post', 'put'])) {
            $house = $this->Properties->patchEntity($houses, $this->request->getData());
            if ($this->Properties->save($house)) {

                //save certificates
                if(!empty($this->request->data['certificate_name']) && $this->request->data['certificate_name'][0] != '')
                {
                    $certis = $this->request->data['certificate_name'];
                    foreach($certis as $certi)
                    {
                        $certificate = $this->PropertyCertificates->newEntity([
                            'property_id'           => $house->id,
                            'certificates_name'     => $certi,
                            'created'               => date("Y-m-d H:i:s"),
                            'modified'              => date("Y-m-d H:i:s"),
                        ]);
                        $saveCerti = $this->PropertyCertificates->save($certificate);
                    }
                }  

                //save development plan
                if(!empty($this->request->data['dev_plan']) && $this->request->data['dev_plan'][0] != '')
                {
                    $devs = $this->request->data['dev_plan'];
                    foreach($devs as $dev)
                    {
                        $dev_plan = $this->PropertyDevelopmentPlans->newEntity([
                            'property_id'           => $house->id,
                            'dev_plan'              => $dev,
                            'created'               => date("Y-m-d H:i:s"),
                            'modified'              => date("Y-m-d H:i:s"),
                        ]);
                        $saveDevPlan = $this->PropertyDevelopmentPlans->save($dev_plan);
                    }
                }

                //save image
                $this->loadModel('PropertyImageFiles');
                if (!empty($this->request->data['logo']['name'])) {
                    $ext = substr(strtolower(strrchr($this->request->data['logo']['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'png'); //set allowed extensions
                    $setNewFileName = time() . "_" . rand(000000, 999999);
                    //only process if the extension is valid
                    $uniquecode = substr(md5(microtime()),0,10); //generate random string
                    $randomKey = substr(md5(microtime()),0,10);
                    $this->request->data['activation_key'] = $uniquecode;
                    if (!empty($ext)) {
                        if (in_array($ext, $arr_ext)) {
                            $image = $this->PropertyImageFiles->newEntity([
                                'property_id'     => $house->id,
                                'file_name'       => $setNewFileName . '.' . $ext,'activation_key'  => $uniquecode,
                                'file_path'       => 'image_file/',
                                'created'         => date("Y-m-d H:i:s"),
                                'modified'        => date("Y-m-d H:i:s"),
                            ]);
                            //do the actual uploading of the file. First arg is the tmp name, second arg is 
                            //where we are putting it
                            move_uploaded_file($this->request->data['logo']['tmp_name'], WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext);
                            $imagine = new \Imagick(WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext);

                            // 写真サイズ取得
                            $img_width  = $imagine->getImageWidth();
                            $img_height = $imagine->getImageHeight();
                                     
                            // 写真リサイズ
                            // 縦サイズ601px以上だったら縦サイズ600pxにリサイズ
                            $max_height = 150;
                            $max_width = 150;
                            if (!empty($img_width) && !empty($img_height) && ($max_height < $img_height) && ($max_width < $img_width)) {
                            // リサイズ処理
                                $imagine->adaptiveResizeImage(150, 150, true);
                            }
                                     
                            // 写真上書き保存
                            if (!$imagine->writeImage(WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext)) {
                            // 上書き保存エラー
                            }
                                     
                            // Imagick clear
                            $imagine->clear();

                            //prepare the filename for database entry 
                            $imageFileName = $setNewFileName . '.' . $ext;
                            $saveImage = $this->PropertyImageFiles->save($image);
                            } else {
                                $this->Flash->error(__('Wrong file type'));
                            }
                    }
                }
                if (!empty($this->request->data['file_name'])) {
                    foreach($this->request->data['file_name'] as $img) {
                    $ext = substr(strtolower(strrchr($img['name'], '.')), 1); //get the extension
                    $arr_ext = array('jpg', 'jpeg', 'png'); //set allowed extensions
                    $setNewFileName = time() . "_" . rand(000000, 999999);
                    //only process if the extension is valid
                    $uniquecode = substr(md5(microtime()),0,10); //generate random string
                    $randomKey = substr(md5(microtime()),0,10);
                    $this->request->data['activation_key'] = $uniquecode;
                    if (!empty($ext)) {
                        if (in_array($ext, $arr_ext)) {
                            $image = $this->PropertyImageFiles->newEntity([
                                'property_id'     => $apartment->id,
                                'file_name'       => $setNewFileName . '.' . $ext,'activation_key'  => $uniquecode,
                                'file_path'       => 'image_file/',
                                'created'         => date("Y-m-d H:i:s"),
                                'modified'        => date("Y-m-d H:i:s"),
                            ]);
                            //do the actual uploading of the file. First arg is the tmp name, second arg is 
                            //where we are putting it
                            move_uploaded_file($img['tmp_name'], WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext);
                            $imagine = new \Imagick(WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext);

                            // 写真サイズ取得
                            $img_width  = $imagine->getImageWidth();
                            $img_height = $imagine->getImageHeight();
                                     
                            // 写真リサイズ
                            // 縦サイズ601px以上だったら縦サイズ600pxにリサイズ
                            $max_height = 150;
                            $max_width = 150;
                            if (!empty($img_width) && !empty($img_height) && ($max_height < $img_height) && ($max_width < $img_width)) {
                            // リサイズ処理
                                $imagine->adaptiveResizeImage(150, 150, true);
                            }
                                     
                            // 写真上書き保存
                            if (!$imagine->writeImage(WWW_ROOT . 'img/' . 'logo/' . $setNewFileName . '.' . $ext)) {
                            // 上書き保存エラー
                            }
                                     
                            // Imagick clear
                            $imagine->clear();

                            //prepare the filename for database entry 
                            $imageFileName = $setNewFileName . '.' . $ext;
                            $saveImage = $this->PropertyImageFiles->save($image);
                            } else {
                                $this->Flash->error(__('Wrong file type'));
                            }
                        }
                    }
                } 


                //save facilities
                if (isset($this->request->data['facility'])) {
                    $facility = $this->request->data['facility'];
                    if(count($selectedCat) == count($facility)){
                        foreach($facility as $facs){
                            $facs = $this->PropertyFacilitiesRelations->newEntity([
                                'property_id'       => $houses->id,
                                'facilities_id'     => $facs,
                                'created'         => date("Y-m-d H:i:s"),
                                'modified'        => date("Y-m-d H:i:s"),
                            ]);
                        $saveFacs = $this->PropertyFacilitiesRelations->save($facs);
                    }
                } else {
                    $query = $this->PropertyFacilitiesRelations->query();
                    $query->delete()
                    ->where(['property_id' => $id])
                    ->execute();
                    foreach($facility as $facs){
                    $facs = $this->PropertyFacilitiesRelations->newEntity([
                        'property_id'       => $houses->id,
                        'facilities_id'     => $facs,
                        'created'         => date("Y-m-d H:i:s"),
                        'modified'        => date("Y-m-d H:i:s"),
                    ]);
                    $saveType = $this->PropertyFacilitiesRelations->save($facs);
                    }
                }
                } else {
                    $query = $this->PropertyFacilitiesRelations->query();
                    $query->delete()
                     ->where(['property_id' => $id])
                    ->execute();
                }

                //save area information
                if(!empty($this->request->data['public_transportation']) || !empty($this->request->data['neighbourhood']) || !empty($this->request->data['industrial_area']) || !empty($this->request->data['supermarket']))
                {
                    $property_area_id = $houses->property_area[0]->id;
                    $area = $this->PropertyArea->get($property_area_id);
                    $areas = $this->PropertyArea->patchEntity($area, $this->request->data);
                    $savarea = $this->PropertyArea->save($areas);   
                }

                $this->Flash->success(__('The house has been saved.'));

                return $this->redirect(['action' => 'houses']);
            }
            $this->Flash->error(__('The house could not be saved. Please, try again.'));
        }
        $this->set(compact('houses', 'provinsis', 'kota', 'facilities', 'selectedCats', 'selectedCat'));
    }

    public function deleteHouse($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $houses = $this->Properties->get($id);
        if ($this->Properties->delete($houses)) {
            $this->Flash->success(__('The house has been deleted.'));
        } else {
            $this->Flash->error(__('The house could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'houses']);
    }

    public function deleteImageHouse($property_id = null, $id = null)
    {
        $this->loadModel('PropertyImageFiles');
        // set default class & message for setFlash
        $class = 'flash_bad';
        $msg   = 'Invalid List Id';
        
        // check id is valid
        if($id!=null && is_numeric($id)) {
            // get the Item
            $item = $this->PropertyImageFiles->find('all', [
            'conditions' => [
                'id'  => $id
            ]
            ])->first();
            
            // check Item is valid
            if(!empty($item)) {
                // try deleting the item
                $item = $this->PropertyImageFiles->get($id);
                if($this->PropertyImageFiles->delete($item)) {
                    $class = 'flash_good';
                    $msg   = 'Your picture was successfully deleted';
                    $this->Flash->success($msg,'default',array('class'=>$class));
                    return $this->redirect(['action'=>'editHouse', $property_id]);
                } else {
                    $msg = 'There was a problem deleting your picture, please try again';
                    $this->Flash->error($msg,'default',array('class'=>$class));
                    return $this->redirect(['action'=>'editHouse', $property_id]);
                }
            }
        }        
    }
    /* Houses -- End */


    /* Facilities -- Start */

    public function facilities()
    {
        $facilities = $this->Facilities->find('all')->toArray();
        $countFacilities = count($facilities);

        //$webdev_category = $this->WebdevCategories->find('all');
        //$portfolio_category = $this->PortfolioCategories->find('all');

        $this->set(compact('facilities', 'countFacilities'));
    }

    public function addFacility()
    {
        $facility = $this->Facilities->newEntity();
        if ($this->request->is('post')) {
            $facility = $this->Facilities->patchEntity($facility, $this->request->getData());
            if ($this->Facilities->save($facility)) {
                $this->Flash->success(__('The facility has been saved.'));
                return $this->redirect(['action' => 'facilities']);
            }
            $this->Flash->error(__('The facility could not be saved. Please, try again.'));
        }
        $this->set(compact('facility'));
    }

    public function editFacility($id = null)
    {
        $facility = $this->Facilities->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $facility = $this->Facilities->patchEntity($facility, $this->request->getData());
            if ($this->Facilities->save($facility)) {
                $this->Flash->success(__('The facility has been updated.'));

                return $this->redirect(['action' => 'facilities']);
            }
            $this->Flash->error(__('The facility could not be updated. Please, try again.'));
        }
        $this->set(compact('facility'));
    }

    public function deleteFacility($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $facility = $this->Facilities->get($id);
        if ($this->Facilities->delete($facility)) {
            $this->Flash->success(__('The facility has been deleted.'));
        } else {
            $this->Flash->error(__('The facility could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'facilities']);
    }

    /* Facilities -- End */


    /* Area Information -- Start */

    public function areaInformation()
    {
        $area = $this->PropertyAreaTypes->find('all')->toArray();
        $transport = $this->PropertyArea->find()
                     ->where(['area_type_id' => 1])
                     ->all();


        $this->set(compact('area', 'transport'));
    }

    public function addTransportation()
    {
        $transportation = $this->PropertyArea->newEntity();
        if ($this->request->is('post')) {
            $this->request->data['area_type_id'] = 1;
            $transportation = $this->PropertyArea->patchEntity($transportation, $this->request->getData());
            if ($this->PropertyArea->save($transportation)) {
                $this->Flash->success(__('The area information has been saved.'));
                return $this->redirect(['action' => 'areaInformation']);
            }
            $this->Flash->error(__('The area information could not be saved. Please, try again.'));
        }
        $this->set(compact('transportation'));
    }

    public function transportDelete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $transportation = $this->PropertyArea->get($id);
        if ($this->PropertyArea->delete($transportation)) {
            $this->Flash->success(__('The transportation information has been deleted.'));
        } else {
            $this->Flash->error(__('The transportation information could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'areaInformation']);
    }

    public function transportEdit($id = null)
    {
        $transportation = $this->PropertyArea->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $transportation = $this->PropertyArea->patchEntity($transportation, $this->request->getData());
                if ($this->PropertyArea->save($transportation)) {
                    $this->Flash->success(__('This transportation information has been updated'));

                    return $this->redirect(['action' => 'areaInformation']);
                }
            $this->Flash->error(__('This transportation information has been updated'));
        }
        $this->set(compact('transportation'));
    }

    /* Area Information -- End */


    public function deleteCertificatesApt($id = null, $id_property = null)
    {
        $certi = $this->PropertyCertificates->get($id);
        if ($this->PropertyCertificates->delete($certi)) {
            $this->Flash->success(__('The certificate has been deleted.'));
        } else {
            $this->Flash->error(__('The certificate could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'editApt', $id_property]);
    }

    public function deleteInvestmentInformation($id = null, $id_property = null)
    {
        $info = $this->PropertyInvestmentInformations->get($id);
        if ($this->PropertyInvestmentInformations->delete($info)) {
            $this->Flash->success(__('The investment information has been deleted.'));
        } else {
            $this->Flash->error(__('The ivestment information could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'editApt', $id_property]);
    }

    public function deleteCertificatesHouse($id = null, $id_property = null)
    {
        $certi = $this->PropertyCertificates->get($id);
        if ($this->PropertyCertificates->delete($certi)) {
            $this->Flash->success(__('The certificate has been deleted.'));
        } else {
            $this->Flash->error(__('The certificate could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'editApt', $id_property]);
    }

    public function deleveDevPlanHouse($id = null, $id_property = null)
    {
        $dev_plan = $this->PropertyDevelopmentPlans->get($id);
        if ($this->PropertyDevelopmentPlans->delete($dev_plan)) {
            $this->Flash->success(__('The dev plan has been deleted.'));
        } else {
            $this->Flash->error(__('The dev plan could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'editApt', $id_property]);
    }

}