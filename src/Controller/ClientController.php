<?php
namespace App\Controller;

use Cake\Event\Event;
use Cake\Auth\DefaultPasswordHasher;
use App\Controller\AppController;

/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link https://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ClientController extends AppController
{

    public function initialize() {
        parent::initialize();
        $this->loadModel("Users");
        $this->loadModel("Provinsi");
        $this->loadModel("Kota");
        $this->loadModel("Facilities");
        $this->loadModel("Types");
        $this->loadModel("Properties");
        $this->loadComponent('Flash');
        $this->viewBuilder()->setLayout('client');
        $this->loadComponent('Search.Prg', [
        // This is default config. You can modify "actions" as needed to make
        // the PRG component work only for specified methods.
        'actions' => ['apartment', 'house']
        ]);

    }

    public function index()
    {

        $this->set(
            'provinsis', 
        $this->Provinsi->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ]));

        $kota = $this->Kota->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ])->contain(['Provinsi']);

        $this->set(
            'facilities', 
        $this->Facilities->find('list', [
        'keyField' => 'id',
        'valueField' => 'facility_name'
        ]));

        $this->set(
            'type', 
        $this->Types->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ]));

        $props = $this->Properties->find('all', [
                'conditions' => [
                ],
                'contain' => ['PropertyImageFiles', 'PropertyArea', 'PropertyCertificates', 'PropertyDevelopmentPlans', 'PropertyFacilitiesRelations.Facilities']
                
            ])->toArray();
        $countProps = count($props);
        $this->set(compact('provinsis', 'kota', 'facilities', 'type', 'props'));
    }

    public function apartment($type = null)
    {

        $this->set(
            'provinsis', 
        $this->Provinsi->find('list', [
        'keyField' => 'slug',
        'valueField' => 'name'
        ]));

        $kota = $this->Kota->find('list', [
        'keyField' => 'slug',
        'valueField' => 'name'
        ])->contain(['Provinsi']);

        $this->set(
            'facilities', 
        $this->Facilities->find('list', [
        'keyField' => 'slug',
        'valueField' => 'facility_name'
        ]));

        $this->set(
            'type', 
        $this->Types->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ]));

        $props = $this->Properties->find('all', [
                'conditions' => [
                    'type' => '1'
                ],
                'contain' => ['PropertyImageFiles', 'PropertyArea', 'PropertyCertificates', 'PropertyDevelopmentPlans', 'PropertyFacilitiesRelations.Facilities']
                
            ])->toArray();
        $countProps = count($props);

        $query = $this->Properties
        // Use the plugins 'search' custom finder and pass in the
        // processed query params
        ->find('search', ['search' => $this->request->query])
        ->order('Properties.id DESC')
        ->contain(['PropertyImageFiles', 'PropertyArea', 'PropertyCertificates', 'PropertyDevelopmentPlans', 'PropertyFacilitiesRelations.Facilities'])
        ->where(['Properties.type' => '1']);

        $this->set(compact('provinsis', 'kota', 'facilities', 'type', 'props', 'query'));
    }

    public function house()
    {

        $this->set(
            'provinsis', 
        $this->Provinsi->find('list', [
        'keyField' => 'slug',
        'valueField' => 'name'
        ]));

        $kota = $this->Kota->find('list', [
        'keyField' => 'slug',
        'valueField' => 'name'
        ])->contain(['Provinsi']);

        $this->set(
            'facilities', 
        $this->Facilities->find('list', [
        'keyField' => 'slug',
        'valueField' => 'facility_name'
        ]));

        $this->set(
            'type', 
        $this->Types->find('list', [
        'keyField' => 'id',
        'valueField' => 'name'
        ]));

        $props = $this->Properties->find('all', [
                'conditions' => [
                    'type' => '2'
                ],
                'contain' => ['PropertyImageFiles', 'PropertyArea', 'PropertyCertificates', 'PropertyDevelopmentPlans', 'PropertyFacilitiesRelations.Facilities']
                
            ])->toArray();
        $countProps = count($props);

        $query = $this->Properties
        // Use the plugins 'search' custom finder and pass in the
        // processed query params
        ->find('search', ['search' => $this->request->query])
        ->order('Properties.id DESC')
        ->contain(['PropertyImageFiles', 'PropertyArea', 'PropertyCertificates', 'PropertyDevelopmentPlans', 'PropertyFacilitiesRelations.Facilities'])
        ->where(['Properties.type' => '1']);

        $this->set(compact('provinsis', 'kota', 'facilities', 'type', 'query'));
    }


    public function propertyDetail($slug = null)
    {
        $detail_property = $this->Properties->find('all', [
            'contain' => ['PropertyImageFiles', 'PropertyArea', 'PropertyCertificates', 'PropertyDevelopmentPlans', 'PropertyFacilitiesRelations.Facilities'],
            'conditions' => ['Properties.slug' => $slug],
        ])->first();   


        $this->set(compact('detail_property'));   
    }

    public function logout()
    {
        $this->Flash->success('You are now logged out');
        return $this->redirect($this->Auth->logout());
    }


    public function ajaxCityId($id = null, $key = null, $cur = null)
    {
        $this->viewBuilder()->setLayout('ajax');
        $this->loadModel('Kota');
        $this->loadModel('Provinsi');
        $id = $this->Provinsi->find('all', [
            'conditions' =>[
                'Provinsi.slug' => $id
            ]
        ])->first();
        $this->set('types', $this->Kota->find('all', array('conditions' => array( 'Kota.province_id =' => $id->id), 'order' => array('Kota.name' => 'asc'))));

        $this->set('key', $key);

        $this->set(compact('types', 'kota'));
        $this->set('_serialize', ['types']);
    }
}
