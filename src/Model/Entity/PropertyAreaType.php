<?php
namespace Manage\Model\Entity;

use Cake\ORM\Entity;

/**
 * Keyword Entity.
 */
class PropertyAreaType extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'name' => false,
    ];
}
