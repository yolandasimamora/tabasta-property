<?php
namespace Manage\Model\Entity;

use Cake\ORM\Entity;

/**
 * Keyword Entity.
 */
class PropertyCertificate extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'property_id' => false,
    ];
}
