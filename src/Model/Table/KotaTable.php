<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Kota Model
 *
 * @property \App\Model\Table\ProvinsiTable|\Cake\ORM\Association\BelongsTo $Provinsi
 *
 * @method \App\Model\Entity\Kotum get($primaryKey, $options = [])
 * @method \App\Model\Entity\Kotum newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Kotum[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Kotum|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Kotum patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Kotum[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Kotum findOrCreate($search, callable $callback = null, $options = [])
 */
class KotaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('kota');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Provinsi', [
            'foreignKey' => 'id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('Properties', [
            'foreignKey' => 'id_kota',
            'dependent' => true,
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['province_id'], 'Provinsi'));

        return $rules;
    }
}
