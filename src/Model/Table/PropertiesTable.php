<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class PropertiesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('property_data');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
        // Add the behaviour to your table
        $this->addBehavior('Search.Search');

        // Setup search filter using search manager
        $this->searchManager()
            ->value('name')
            // Here we will alias the 'q' query param to search the `Articles.title`
            // field and the `Articles.content` field, using a LIKE match, with `%`
            // both before and after.
            ->add('q', 'Search.Like', [
                'before' => true,
                'after' => true,
                'fieldMode' => 'OR',
                'comparison' => 'LIKE',
                'wildcardAny' => '*',
                'wildcardOne' => '?',
                'field' => ['title', 'content']
            ])
            ->add('province', 'Search.Callback', [
                'callback' => function ($query, $args, $filter) {
                    return $query
                    ->distinct($this->aliasField('type'))
                    ->matching('Provinsi', function (Query $query) use ($args) {
                        return $query
                            ->where([
                                'Provinsi.slug' => $args['province']
                            ]);
                    });

                }
            ])
            ->add('city', 'Search.Callback', [
                'callback' => function ($query, $args, $filter) {
                    return $query
                    ->distinct($this->aliasField('type'))
                    ->matching('Kota', function (Query $query) use ($args) {
                        return $query
                            ->where([
                                'Kota.slug' => $args['city']
                            ]);
                    });

                }
            ])
            ->add('facility', 'Search.Callback', [
                'callback' => function ($query, $args, $filter) {
                    return $query
                    ->distinct($this->aliasField('type'))
                    ->matching('PropertyFacilitiesRelations.Facilities', function (Query $query) use ($args) {
                        return $query
                            ->where([
                                'Facilities.slug' => $args['facility']
                            ]);
                    });

                }
            ]);

        $this->hasMany('PropertyImageFiles', [
            'dependent' => true,
            'foreignKey' => 'property_id',
        ]);

        $this->hasMany('PropertyFacilitiesRelations', [
            'dependent' => true,
            'foreignKey' => 'property_id',
        ]);

        $this->hasMany('PropertyArea', [
            'dependent' => true,
            'foreignKey' => 'property_id',
        ]);

        $this->hasMany('PropertyCertificates', [
            'dependent' => true,
            'foreignKey' => 'property_id',
        ]);

        $this->hasMany('PropertyDevelopmentPlans', [
            'dependent' => true,
            'foreignKey' => 'property_id',
        ]);

        $this->hasMany('PropertyInvestmentInformations', [
            'dependent' => true,
            'foreignKey' => 'property_id',
        ]);

        $this->hasMany('PropertyAptPriceByTypes', [
            'dependent' => true,
            'foreignKey' => 'property_id',
        ]);

        $this->belongsTo('Provinsi', [
            'foreignKey' => 'provinsi_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Kota', [
            'foreignKey' => 'kota_id',
            'joinType' => 'INNER'
        ]);

        $this->belongsTo('Types', [
            'foreignKey' => 'type_property',
            'joinType' => 'INNER'
        ]);

    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 150)
            ->requirePresence('name', 'create')
            ->notEmpty('name');


        return $validator;
    }
}
