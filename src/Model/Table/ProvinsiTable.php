<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Provinsi Model
 *
 * @method \App\Model\Entity\Provinsi get($primaryKey, $options = [])
 * @method \App\Model\Entity\Provinsi newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Provinsi[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Provinsi|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Provinsi patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Provinsi[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Provinsi findOrCreate($search, callable $callback = null, $options = [])
 */
class ProvinsiTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->hasMany('Kota', [
            'foreignKey' => 'id_provinsi',
            'dependent' => true,
        ]);
        $this->hasMany('Properties', [
            'foreignKey' => 'id_provinsi',
            'dependent' => true,
        ]);

        $this->setTable('provinsi');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->allowEmpty('id', 'create');

        $validator
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }
}
