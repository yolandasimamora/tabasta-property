
<!-- 
<div class="hold-transition login-page"> -->
    <div class="login-box">
        <div class="login-logo">
            <a href="#"><b>Tabasta</b> Admin</a>
        </div>
        <div class="login-box-body">
            <p class="login-box-msg">Sign in to start your session</p>
            <?= $this->Flash->render('auth') ?>
            <?= $this->Form->create() ?>
                <div class="form-group has-feedback">
                    <?= $this->Form->input('username', ['class' => 'form-control']) ?>
                    <span class="fa fa-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <?= $this->Form->input('password', ['class' => 'form-control']) ?>
                    <span class="fa fa-lock form-control-feedback"></span>
                </div>
                <?php use Cake\Core\Configure; ?>
                    <div class="form-group has-feedback">
                    <div class="g-recaptcha" data-sitekey="<?php echo Configure::read('google_recatpcha_settings.site_key'); ?>"></div>
                <div class="row">
                    <div class="col-xs-12">
                       <center> <?= $this->Form->button(__('Sign In'),['class' => 'btn btn-md btn-success', 'style' => 'margin-top: 10px;']); ?></center
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </form>
        </div>
    </div>
<!-- </div> -->