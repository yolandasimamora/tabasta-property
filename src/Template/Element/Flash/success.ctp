<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="background-message"  onclick="this.classList.add('hidden');">
	<div class="message success" onclick="this.classList.add('hidden')"><?= $message ?></div>
</div>