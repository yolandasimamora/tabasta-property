        <div class="wrapper">
            <header>
   <!-- Navigation -->
    <nav class="navbar-head" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header" style = "margin-top:25px;">
            <?php echo $this->Html->image("tabasta.png", ["alt" => "tabasta logo",'url' => ['controller' => 'Client', 'action' => 'index'],'class' => 'img-responsive', 'style' => 'height:70px;']);?>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
             <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li>
                        <a href="<?= $this->Url->build(["controller" => "Client", "action" => "index"]); ?>">Home</a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(["controller" => "Client", "action" => "apartment"]); ?>">Apartment</a>
                    </li>
                    <li>
                        <a href="<?= $this->Url->build(["controller" => "Client", "action" => "house"]); ?>">House</a>
                    </li>
                    <li>
                        <a href="galeri.html">Galeri</a>
                    </li>
                    <li>
                        <a href="about.html">Tentang Kami</a>
                    </li>
                    <li>
                        <a href="contact.html">Kontak</a>
                    </li>
                </ul>
            </div> 
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
            </header>