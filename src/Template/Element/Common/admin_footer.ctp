<?php
	$this->request->session()->read('Auth.User');
	$session_user = $this->request->session()->read('Auth.User');
	if(!empty($session_user)){ ?>
	     <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>WDI</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2018 <a href="https://logique.co.id">TABASTA PROPERTY</a>.</strong> All rights
    reserved.
  </footer>   
	<?php }
	else
	{ ?>
	    
	<?php }
	?>

<script src="<?php echo $this->Url->script('jquery.min.js');?>"></script>
<script src="<?php echo $this->Url->script('bootstrap.min.js');?>"></script>
<script src="<?php echo $this->Url->script('bootstrap-datepicker.min.js');?>"></script>
 