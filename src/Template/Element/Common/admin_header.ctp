<body class="hold-transition skin-black-light sidebar-mini">
<div class="wrapper">
<?php use Cake\I18n\Time;?>
<?php 
  $this->request->session()->read('Auth.User');
  $session_user = $this->request->session()->read('Auth.User');
  if(!empty($session_user)){ ?>
<header class="main-header">
<!--     <a href="index2.html" class="logo">
      <span class="logo-mini"><b>WDI</b></span>
      <span class="logo-lg"><b>WDI</b> Admin</span>
    </a> -->

    <?= $this->Html->link(
    '<span class="logo-mini"><b>WDI</b></span><span class="logo-lg"><b>Tabasta</b> Admin</span>', 
    '/tabastaadmin/', 
    [
        'escape' => false, 
        'class' => 'logo'
    ]
    ) ?>
    <nav class="navbar navbar-static-top">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="<?php echo $this->Url->image('user2-160x160.jpg');?>" class="user-image" alt="">  

              <span class="hidden-xs"><?= h($userinfo->name)?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img src="<?php echo $this->Url->image('user2-160x160.jpg');?>" class="img-circle" alt="">

                <p>
                  <?= h($userinfo->name)?>
                  <?php $now = Time::parse($userinfo->created);?>
                  <small>Member since <?= h($now->nice()); ?></small>
                </p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?= $this->Url->build(["controller" => "Tabastaadmin", "action" => "adminProfile", $userinfo->id]); ?>">Profile</a>
                </div>
                <div class="pull-right">
                  <!-- <a href="#" class="btn btn-default btn-flat">Sign out</a> -->
                  <?=$this->Html->Link(__('Log out'),['controller'=>'Tabastaadmin','action'=>'logout']) ?>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <?php }
  else
  { ?>
      <header></header>
  <?php }
  ?>

 
 