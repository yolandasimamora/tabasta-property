<aside class="main-sidebar">
  <section class="sidebar">
    <ul class="sidebar-menu" data-widget="tree">
      <li class="active">
      <?= $this->Html->link(
		    '<i class="fa fa-dashboard"></i><span>Dashboard</span>', 
		    '/tabastaadmin/index', 
		    [
		        'escape' => false
		    ]
		) ?>
	  </li>
      <li>
      <?= $this->Html->link(
		    '<i class="fa fa-building-o"></i><span>Apartments</span>', 
		    '/tabastaadmin/apartments', 
		    [
		        'escape' => false
		    ]
		) ?>
      </li>
      <li>
      <?= $this->Html->link(
		    '<i class="fa fa-building"></i><span>Houses</span>', 
		    '/tabastaadmin/houses', 
		    [
		        'escape' => false
		    ]
		) ?>
      </li>
      <li>
      <?= $this->Html->link(
		    '<i class="fa fa-shopping-bag"></i><span>Facilities</span>', 
		    '/tabastaadmin/facilities', 
		    [
		        'escape' => false
		    ]
		) ?>
      </li>
      <li>
      <?= $this->Html->link(
		    '<i class="fa fa-subway"></i><span>Area Information</span>', 
		    '/tabastaadmin/area-information', 
		    [
		        'escape' => false
		    ]
		) ?>
      </li>
    </ul>
  </section>
</aside>