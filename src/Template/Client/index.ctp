<section class="main-banner-cont">
    <div class="main-banner-color">
        <div class="container">
            <div class="banner-caption">
            <h1>Temukan Properti Terbaik dan Terlengkap di Indonesia</h1>
            <div class="border"></div>
                 <div class="banner-menu-container">
                    <ul>
                        <li class="search-webdev"><?=$this->Html->link( 'Find Apartment Property', 
                            ['controller' => 'Client', 'action' => 'apartment', '_full' => true]
                        );?>
                        </li>
                        <li  class="register-webdev"><?=$this->Html->link( 'Find Housing Property',
                            ['controller' => 'Client', 'action' => 'house', '_full' => true]
                        );?>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<br>
<section>
    <div class = "col-md-12">
    <h2 class="title">Latest Properties</h2>
    <?php foreach($props as $prop):?>
        <div class="col-sm-4 col-lg-4 col-md-4">
            <div class="thumbnail">
                <?php if(!empty($prop->property_image_files)):?>
                    <?= $this->Html->image('logo/'.$prop->property_image_files[0]['file_name'],array('class'=>'media-‌​‌​object img img-thumbnail thumb-img','alt'=>$prop->property_image_files[0]['file_name']) );?>
                <?php else:?>
                    <img src="http://placehold.it/320x150" alt="">
                <?php endif;?>
                    <div class="caption">
                        <h4 class="pull-right"><?php echo h($prop->price);?></h4>
                        <h4><a href="#"><?php echo h($prop->name);?></a></h4>
                        <p><?php echo h($prop->address);?></p>
                    </div>
                    <div class="ratings">
                        <p class="pull-right">15 reviews</p>
                        <p>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                        </p>
                    </div>
            </div>
        </div>
    <?php endforeach;?>
    </div>
</section>