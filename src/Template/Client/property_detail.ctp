<!--  -->
<section class="staticpage-banner-cont aboutus-banner">
				<div class="staticpage-banner-color">
				<div class="container">
					<div class="banner-caption">
					 
				</div>
				</div>
			</section>
			<section class="breadcrumb-section">
				<div class="container">
<!-- 					<div class="breadcrumb-cont">
						<ul>
							<li><a href="<?= $this->Url->build(["controller" => "Client", "action" => "index"]); ?>">Beranda</a></li>
							<li><a href="<?= $this->Url->build(["controller" => "Client", "action" => "findWebdev"]); ?>">Web Developer</a></li>
							<li><a  class="capitalize" href="<?= $this->Url->build(["controller" => "Client", "action" => "findWebdevByProvince", $detail_webdev->provinsi->slug]); ?>"><?= h($detail_webdev->provinsi->slug) ?></a></li>
							<li><a  class="capitalize" href="<?= $this->Url->build(["controller" => "Client", "action" => "../webdev",$detail_webdev->provinsi->slug, $detail_webdev->kotum->slug]); ?>"> <?= h($detail_webdev->kotum->name) ?> </a></li>
							<li class="capitalize" ><?= h($detail_webdev->slug) ?></li>
						</ul>
					</div> -->
				</div>
			</section>
			<section class="content-container">
				<div class="container">
					<div class="row">
						<div class="col-sm-9">	 
							<div class="newsdetail-container">
								<div class="wevdev-header">
								<div class=row>
									<div class="col-sm-2">
										<div class="detailnews-img"><?php   if (!empty($detail_property->property_image_files[0]['file_name'])):?>
												<?= $this->Html->image('logo/'.$detail_property->property_image_files[0]['file_name'],array('class'=>'media-‌​‌​object img img-thumbnail','alt'=>$detail_property->name) );?>
												<?php endif; ?>
											</div>
									</div>
									<div class="col-sm-10">
										<div class="newsdetail-title">
											<h1><?= h($detail_property->name) ?></h1>
<!-- 											   	<ul class="list-webdev-type type-list">
													<?php foreach($detail_property->webdev_category_relations as $type):?>
                										<li>
                										<?php 
														echo $this->Html->link(
														    __($type->detail_property->name),
														    array(
														        'controller' => 'Client',
														        'action' => 'findWebdevByCat', 
														        $type->detail_property->slug
														    ),
														    array('style' => "color:#fff;", 'id' => "")
														);
														?>

                										</li>
											    	<?php endforeach;?>
											    </ul> -->
<!-- 											   	<div class="from-province">
											   		<strong><i class="fa fa-map-marker" aria-hidden="true"></i></strong> 
											   		 <?php 
                              					        echo ucwords($city); echo ', '.ucwords($provinsi).' ';
                              					        ?>
											   	</div> -->
										</div>
									</div>
								</div>
								</div>
								<div class="webdev-content">
	 								<div class="nav-tabs-custom">
							            <ul class="nav nav-tabs">
							              <li class="active"><a href="#tab_1" data-toggle="tab">Property Data</a></li>
							              <li><a href="#tab_2" data-toggle="tab">Property Area  </a></li>
							            </ul>
							            <div class="tab-content">
							              <div class="tab-pane active" id="tab_1"><div class="webdev-item">
										<div class="row">
											<div class="col-sm-3">
												<h2>Tentang Kami</h2>
											</div>
											<div class="col-sm-9">
												<div class="webdev-data">
													<div class="row  small-gap">
														<div class="col-sm-3">Email :</div>
														<div class="col-sm-6"> <?= h($detail_property->email) ?></div>
													</div> 
													<div class="row">
														<div class="col-sm-3">Telp Perusahaan :</div>
														<div class="col-sm-6"> <?= h($detail_property->no_telp_kantor) ?></div>
						                            </div>
						                            <div class="row">
														<div class="col-sm-3">Contact Person:</div>
														<div class="col-sm-6"><?= h($detail_property->no_hp) ?></div>
						                            </div>
						                            <div class="row">
														<div class="col-sm-3">Website:</div>
														<div class="col-sm-7">
														<?php if(substr($detail_property->website, 0, 7) == 'http://' || substr($detail_property->website, 0, 8) == 'https://'): ?>
														<?= $this->Html->link(
									                        __($detail_property->website), 
									                        $detail_property->website.'/', 
									                        array('class' => 'button', 'target' => '_blank')
									                     );?>
														<?php else:?>
														<?= $this->Html->link(
									                        __($detail_property->website), 
									                        'http://'.$detail_property->website.'/', 
									                        array('class' => 'button', 'target' => '_blank')
									                     );?>
									                    <?php endif;?>
									                     </div>
						                            </div>
						                             
						                            <div class="row">
														<div class="col-sm-12">Alamat:</div>
														<div class="col-sm-12"> <?php echo html_entity_decode($detail_property->alamat) ?></div>
						                            </div>   
	                            
												</div>
											</div>
										</div>
									</div> 
									</div>
							              <div class="tab-pane" id="tab_2"> 
							              	<div class="row webdev-portfolio-cont">
							              	<?php $rowCount=0; 
							              	foreach($detail_webdev->portfolios as $portfolio):?>

								                 <div class="col-sm-4 portfolio-item">
								                  <div class="portfolio-img">
								                    <?= $this->Html->image('image_file/'.$portfolio['file_name'],array('class'=>'media-‌​‌​object img img-thumbnail','alt'=>$portfolio['file_name'], 'onclick' => 'openModals();currentSlide('.$portfolio->id.')') );?>
								                    
								                  </div>
								                  <div class="text-center">
					                                <h4><?= h($portfolio->caption) ?></h4>
					                                <ul class="portfolio-list-type  type-list">
					                                  <?php foreach($portfolio->portfolio_category_relations as $port):?>
					                                    <li><i class="fa fa-tag"></i> <?= h($port->portfolio_category->name) ?></li>
					                                  <?php  endforeach;?> 
					                                </ul>
					                              </div>
								                </div> 
								                <?php  $rowCount++;
						                            if(($rowCount % 3) == 0 ){
						                              echo '<div class="clearfix"></div><br>';
						                            }

						                            ?>
								              <?php endforeach;?>
								          </div>
								          </div>
							          	</div>
							        </div>

						      </div>
							</div>  						 
						</div>
					</div>
				</div>
			</section>
	<div id="myModals" class="modal portfolio-img">
  
  <div class="modal-content">
 <span class="close cursor" onclick="closeModal()">&times;</span>
  <?php foreach ($detail_webdev->portfolios as $portfolio):?>
    <div class="mySlides">
      <?= $this->Html->image('image_file/'.$portfolio['file_name'],array('style'=>'width:100%','alt'=>$portfolio['file_name']) );?>
    </div>
  <?php endforeach;?>
    
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>

    <div class="caption-container">
      <p id="caption"></p>
    </div>

    <?php foreach ($portfolio as $port):?>
    <div class="column">
      <?= $this->Html->image('image_file/'.$portfolio['file_name'],array('class'=>'demo cursor','style'=>'width:100%' ,'alt'=>$portfolio['file_name'], 'onclick' => 'currentSlide('.$portfolio->id.')') );?>
    </div>
    <?php endforeach;?>
  </div>
</div>