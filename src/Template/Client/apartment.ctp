    <section class="staticpage-banner-cont aboutus-banner">
        <div class="staticpage-banner-color">
            <div class="container">
                <div class="banner-caption">
                </div>
            </div>
        </div>
    </section>
    <section class="breadcrumb-section">
        <div class="container">
            <div class="breadcrumb-cont">
                <ul>
                    <li><a href="<?= $this->Url->build(["controller" => "Client", "action" => "index"]); ?>">Beranda</a></li>
                    <li>Find Apartment</li>
                </ul>
            </div>
        </div>
    </section>
    <!-- Page Content -->
    <div class="container">
    <!-- Search Form -->
    <div class = "col-md-12">
        <h2 class="title">Apartment</h2>
        <p class="description">Anda dapat melakukan pencarian property dijual dengan mengisi form di bawah ini.</p>

        <?= $this->Form->create('', ['valueSources' => 'query']);?>

        <div class="col-sm-3">
            <?= $this->Form->input('province',array('type'=>'select','empty'=>'- Province -','options'=>$provinsis,'id'=>'province_id','class'=>'form-control form-control-custom','onChange'=>'provChange()', 'label' => false)); ?>
        </div>
        <div class="col-sm-3">
            <?= $this->Form->input('city',array('type'=>'select','empty'=>'- City -','options'=>$kota,'id'=>'city_id','class'=>'form-control form-control-custom', 'label' => false)); ?>
        </div>
        <div class="col-sm-3">
            <?= $this->Form->input('facility',array('type'=>'select','empty'=>'- Facility -','options'=>$facilities,'id'=>'facilities','class'=>'form-control form-control-custom', 'label' => false)); ?>
        </div>
        <div class="col-sm-3">
            <?= $this->Form->input('id_kota',array('type'=>'select','empty'=>'- Price From -','options'=>$type,'id'=>'type','class'=>'form-control form-control-custom', 'label' => false)); ?>
        </div>
        <div class="col-sm-3">
            <?= $this->Form->input('id_kota',array('type'=>'select','empty'=>'- Price To -','options'=>$type,'id'=>'type','class'=>'form-control form-control-custom', 'label' => false)); ?>
        </div>
        <div class="col-sm-3">
            <?= $this->Form->input('id_kota',array('type'=>'select','empty'=>'- Bed Rooms -','options'=>$type,'id'=>'type','class'=>'form-control form-control-custom', 'label' => false)); ?>
        </div>

        <div class="col-sm-3">
            <?= $this->Form->input('id_kota',array('type'=>'select','empty'=>'- Bath Rooms -','options'=>$type,'id'=>'type','class'=>'form-control form-control-custom', 'label' => false)); ?>
        </div>
        <div class="col-sm-3 right">
            <?= $this->Form->button('Search', array('type' => 'submit','class' => 'btn button-search', 'label' => false)); ?>
        </div>
        <?= $this->Form->end();?>

        <div class="emBeli form large-9 medium-8 columns content">
        </div>
    </div>


    <div class = "col-md-12">
    <h2 class="title">Properties</h2>
    <?php foreach($query as $prop):?>
        <div class="col-sm-4 col-lg-4 col-md-4">
            <div class="thumbnail">
                <?php if(!empty($prop->property_image_files)):?>
                    <?= $this->Html->image('logo/'.$prop->property_image_files[0]['file_name'],array('class'=>'media-‌​‌​object img img-thumbnail thumb-img','alt'=>$prop->property_image_files[0]['file_name']) );?>
                <?php else:?>
                    <img src="http://placehold.it/320x150" alt="">
                <?php endif;?>
                    <div class="caption">
                        <h4 class="pull-right"><?php echo h($prop->price);?></h4>
                        <h4><a href="<?= $this->Url->build(["controller" => "Client", "action" => "propertyDetail", $prop->slug]);?>" class=""><?php echo h($prop->name);?></a></h4>
                        <p><?php echo h($prop->address);?></p>
                    </div>
                    <div class="ratings">
                        <p class="pull-right">15 reviews</p>
                        <p>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                            <span class="glyphicon glyphicon-star"></span>
                        </p>
                    </div>
            </div>
        </div>
    <?php endforeach;?>
    </div>

    </div>
    <!-- /.container -->