 <?= $this->element('Common/admin_sidebar') ?>  
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        House Management | Add New House
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">House Management</a></li>
        <li class="active">Add New</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
            <?= $this->Form->create('', ['name' => 'form_wizard', 'class' => 'contactform requestcertificate-form', 'enctype' => 'multipart/form-data']) ?>
              <?php echo $this->Form->hidden('type_property', ['value'=>2, 'name'=>'type_property']);?>
              <div class="box-body">
                <div class="box-header with-border">
                  <h3 class="box-title">House Basic Information</h3>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Logo</label>
                          <br>
                          <div id="imagePreview" alt="" class="img-responsive"><img src="http://via.placeholder.com/250x250" alt="" class="img-responsive"></div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Upload Image</label>
                          <?php  
                              echo $this->Form->input('logo', [
                                  'onchange'=>'return fileValidation()',
                                  'id' => 'file',
                                  'class' => 'form-control',
                                  'type' => 'file',
                                  'label' => false,
                              ]);
                          ?>
                        </div>
                      </div>
                    </div>
                    <div id = 'dynamic_field_2'>
                      <button type="button" name="add_2" id="add_2" class="btn btn-success">Add More</button>
                    </div>
                    <div class="form-group">
                      <label>House Name</label><span style ="color:red;"> *</span>
                      <?= $this->Form->input('name', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'required' => true]); ?>
                    </div>
                    <div class="form-group">
                      <label>Price</label><span style ="color:red;"> *</span>
                      <?= $this->Form->input('price', ['class' => 'form-control', 'label' => false, 'type' => 'number', 'required' => true]); ?>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Province</label><span style ="color:red;"> *</span>
                          <?= $this->Form->input('provinsi_id',array('type'=>'select','empty'=>'- Pilih Provinsi -','options'=>$provinsis,'id'=>'province_id','class'=>'form-control form-control-custom','onChange'=>'provChange()', 'label' => false, 'required' => true, 'value' => '')); ?>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>City</label><span style ="color:red;"> *</span>
                          <?= $this->Form->input('kota_id',array('type'=>'select','empty'=>'- Pilih Kota -','options'=>$kota,'id'=>'city_id','class'=>'form-control form-control-custom', 'label' => false, 'required' => false)); ?>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Address</label><span style ="color:red;"> *</span>
                      <?= $this->Form->input('address', ['class' => 'form-control', 'label' => false, 'type' => 'textarea', 'required' => true]); ?>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Phone</label>
                      <?= $this->Form->input('telephone', ['class' => 'form-control', 'label' => false, 'type' => 'number']); ?>
                    </div>
                    <div class="form-group">
                      <label>Website</label>
                      <?= $this->Form->input('website', ['class' => 'form-control', 'label' => false, 'type' => 'text']); ?>
                    </div>
                    <div class="form-group">
                      <label>House Facility</label>
                        <?php foreach($facilities as $facility): ?>
                           <tr>
                              <td><?= $this->Form->input('facilities_id[]', array('type'=>'checkbox', 'value'=>$facility->id, 'label' => $facility->facility_name,'hiddenField' => false, 'id' => 'type-id'.$facility->id)); ?> </td>
                           </tr>
                          <?php endforeach; ?>
                    </div>
                    <div class="form-group">
                      <label>Developer</label><span style ="color:red;"> *</span>
                      <?= $this->Form->input('developer', ['class' => 'form-control summernote', 'label' => false, 'type' => 'text', 'required' => true]); ?>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="box-header with-border">
                  <h3 class="box-title">House Area Information</h3>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <label>Public Transportation</label><span style ="color:red;"> *</span>
                      <?= $this->Form->input('public_transportation', ['class' => 'form-control', 'label' => false, 'type' => 'textarea', 'required' => true]); ?>
                    </div>
                    <div class="col-md-6">
                      <label>Neighbourhood</label><span style ="color:red;"> *</span>
                      <?= $this->Form->input('neighbourhood', ['class' => 'form-control', 'label' => false, 'type' => 'textarea']); ?>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <label>Industrial Area</label><span style ="color:red;"> *</span>
                      <?= $this->Form->input('industrial_area', ['class' => 'form-control', 'label' => false, 'type' => 'textarea']); ?>
                    </div>
                    <div class="col-md-6">
                      <label>Supermarket</label><span style ="color:red;"> *</span>
                      <?= $this->Form->input('supermarket', ['class' => 'form-control', 'label' => false, 'type' => 'textarea']); ?>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="box-header with-border">
                  <h3 class="box-title">Certificates</h3>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <div id = 'dynamic_field'>
                      <label></label>
                      <?= $this->Form->input('certificate_name', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'name' => 'certificate_name[]']); ?>
                      </div>
                      <button type="button" name="add" id="add" class="btn btn-success">Add More</button>
                    </div>
                    <div class="col-md-6">
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="box-header with-border">
                  <h3 class="box-title">Development Plan</h3>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <div id = 'dynamic_field_1'>
                      <label></label>
                      <?= $this->Form->input('dev_plan', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'name' => 'dev_plan[]']); ?>
                      </div>
                      <button type="button" name="add_1" id="add_1" class="btn btn-success">Add More</button>
                    </div>
                    <div class="col-md-6">
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="box-header with-border">
                  <h3 class="box-title">Financial Information</h3>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <label></label>
                      <?= $this->Form->input('dev_plan', ['class' => 'form-control', 'label' => false, 'type' => 'text']); ?>
                    </div>
                    <div class="col-md-6">
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <a href="company.php" class="btn btn-default"><i class="fa fa-chevron-left"></i> Back</a>
                <button type="submit" class="btn btn-success pull-right" id="button"><i class="fa fa-save"></i> Save</button>
                <?= $this->Form->end(); ?>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>