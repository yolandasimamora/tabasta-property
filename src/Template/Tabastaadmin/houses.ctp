  <?= $this->element('Common/admin_sidebar') ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Houses Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Houses Management</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default color-palette-box">
            <div class="box-header with-border">
              <h3 class="box-title">Houses List</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <?= $this->Html->link(
                    '<i class="fa fa-plus-circle"></i><span> Add New</span>', 
                    '/tabastaadmin/add-house', 
                    [
                        'escape' => false,
                        'class' => 'btn btn-sm btn-success'
                    ]
                ) ?>
                  <br><br>
                  <table id="table" class="table table-bordered table-hover dataTable">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Name</th>
                          <th>Address</th>
                          <th width="20%">Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                      <?php foreach($houses as $house):?>
                        <tr>
                          <td><?= h($house->id) ?></td>
                          <td><?= h($house->name) ?></td>
                          <td><?= h($house->address) ?></td>
                          <td>
                            <div class="btn-group">
                              <?= $this->Html->link(
                                  '<i class="fa fa-eye"></i><span> View</span>', 
                                  '/tabastaadmin/house-view/'.$house->id, 
                                  [
                                      'escape' => false,
                                      'class' => 'btn btn-xs btn-default'
                                  ]
                              ) ?>
                              <?= $this->Html->link(
                              '<i class="fa fa-edit"></i><span> Edit</span>', 
                              '/tabastaadmin/edit-house/'.$house->id, 
                              [
                                  'escape' => false,
                                  'class' => 'btn btn-xs btn-default'
                              ]
                              ) ?>
                              <?= $this->Form->postLink(__('<i class="fa fa-trash"></i> Delete'), ['action' => 'deleteHouse', $house->id], ['confirm' => __('Are you sure you want to delete # {0}?', $house->id), 'escape' =>false, 'class' => 'btn btn-xs btn-danger']) ?>
                            </div>
                          </td>
                        </tr>
                      <?php endforeach;?>
                      </tbody>
                    </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>