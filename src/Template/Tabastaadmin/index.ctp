 <?= $this->element('Common/admin_sidebar') ?> 
  
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Dashboard
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3><?php echo h($apts);?></h3>

              <p>Apartments</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <?= $this->Html->link(
                    '<i class="fa fa-arrow-circle-right"></i><span> Detail</span>', 
                    '/tabastaadmin/apartments', 
                    [
                        'escape' => false,
                        'class' => 'small-box-footer'
                    ]
                ) ?>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3><?php echo h($house);?></h3>

              <p>Houses</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <?= $this->Html->link(
                    '<i class="fa fa-arrow-circle-right"></i><span> Detail</span>', 
                    '/tabastaadmin/houses', 
                    [
                        'escape' => false,
                        'class' => 'small-box-footer'
                    ]
                ) ?>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-12">
          <!-- Lorem ipsum dolor sit amet, consectetur adipisicing elit. Necessitatibus unde sit itaque quis dolorem quae, quidem nobis voluptates facere aliquam velit dolorum, id natus hic sed quam. Harum, veritatis, incidunt! -->
        </div>
      </div>

    </section>
  </div>