 <?= $this->element('Common/admin_sidebar') ?>  
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Job Management | View
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="company.php">Job Management</a></li>
        <li class="active">View</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default color-palette-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?= h($info->property->name) ?></h3>
            </div>  
            <div class="box-body">
              <div class="row">
                <div class="col-sm-4">
                  <div class="job_info content-profile">
                    <dl class="dl-horizontal">
                      <dt>Apartment Unit</dt>
                      <dd><?= h($info->apt_unit)?></dd>
                      <dt>Area Unit</dt>
                      <dd><?= h($info->area)?></dd>
                      <dt>Average Selling Price</dt>
                      <dd><?= h($info->avg_selling_price) ?></dd>
                      <dt>Selling Price per m2</dt>
                      <dd><?= h($info->selling_price_per_m2) ?></dd>
                      <dt>Avg. Rental Price (Monthly)</dt>
                      <dd><?= h($info->avg_rental_price_monthly) ?></dd>
                    </dl>
                  </div>
                </div>
                <div class="col-sm-8 content-profile">
                  <dl class="dl-horizontal">
                      <dt>Avg. Rental Price (Annually)</dt>
                      <dd><?= h($info->avg_rental_price_annually) ?></dd>
                      <dt>Avg. Rental Price Anually (%) </dt>
                      <dd><?= h($info->avg_rental_price_annually_percentage) ?>%</dd>
                      <dt>Service Charge Annually</dt>
                      <dd><?= h($info->service_charge_annually) ?></dd>
                      <dt>Agent Fee Annually</dt>
                      <dd><?= h($info->agent_fee_annually) ?></dd>
                      <dt>Expected Annual Return of Investment Nett of Service Charge & Agent Free (%)</dt>
                      <dd><?= h($info->expected_annual_return) ?>%</dd>
                </dl>
                </div>
              </div>
            </div>

           <div class="box-footer">
              <div class="pull-left">
                <?= $this->Html->link(
                    '<i class="fa fa-chevron-left"></i><span> Back</span>', 
                    '/tabastaadmin/edit-apt/'.$info->property_id.'/'.'#tab_2',
                    //.$info->member->id, 
                    [
                        'escape' => false,
                        'class' => 'btn btn-default'
                    ]
                ) ?>
              </div>
              <div class="pull-right">
                <!-- <?= $this->Form->postLink(__('Approve'), ['action' => 'approveJob', $info->id], ['confirm' => __('Are you sure you want to approve # {0}?', $info->id), 'class' => 'btn btn-success']) ?> -->
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>