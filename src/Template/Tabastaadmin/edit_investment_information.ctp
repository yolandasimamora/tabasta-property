  <?= $this->element('Common/admin_sidebar') ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Investment Information Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Investment Information Management</a></li>
        <li class="active">Add New</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default color-palette-box">
            <div class="box-header with-border">
              <h3 class="box-title">Add New Investment Information</h3>
            </div>
            <?= $this->Form->create('', ['name' => 'form_wizard', 'class' => '', 'enctype' => 'multipart/form-data']) ?>
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Apartment Unit</label>
                    <?= $this->Form->input('apt_unit', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'id' => 'apt_unit', 'value' => $info->apt_unit]); ?>
                  </div>
                  <div class="form-group">
                    <label>Area</label>
                    <?= $this->Form->input('area', ['class' => 'form-control calculate', 'label' => false, 'type' => 'text', 'id' => 'area', 'name' => 'area', 'value' => $info->area]); ?>
                  </div>
                  <div class="form-group">
                    <label>Average Selling Price</label>
                    <?= $this->Form->input('avg_selling_price', ['class' => 'form-control calculate', 'label' => false, 'type' => 'text', 'id' => 'avg_selling_price', 'name' => 'avg_selling_price', 'value' => $info->avg_selling_price]); ?>
                  </div>
                  <div class="form-group">
                    <label>Selling Price per m2</label>
                    <?= $this->Form->input('selling_price_per_m2', ['class' => 'form-control calculate', 'label' => false, 'type' => 'text', 'id' => 'selling_price_per_m2', 'name' => 'selling_price_per_m2', 'value' => $info->selling_price_per_m2]); ?>
                  </div>
                  <div class="form-group">
                    <label>Average Rental Price (monthly)</label>
                    <?= $this->Form->input('avg_rental_price_monthly', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'id' => 'avg_rental_price_monthly', 'name' => 'avg_rental_price_monthly', 'value' => $info->avg_rental_price_monthly]); ?>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label>Average Rental Price (annually)</label>
                    <?= $this->Form->input('avg_rental_price_annually', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'id' => 'avg_rental_price_annually', 'name' => 'avg_rental_price_annually', 'value' => $info->avg_rental_price_annually]); ?>
                  </div>
                  <div class="form-group">
                    <label>Average Rental Price Annually (%)</label>
                    <?= $this->Form->input('avg_rental_price_annually_percentage', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'id' => 'avg_rental_price_annually_percentage', 'name' => 'avg_rental_price_annually_percentage', 'value' => $info->avg_rental_price_annually_percentage]); ?>
                  </div>
                  <div class="form-group">
                    <label>Service Charge (annually)</label>
                    <?= $this->Form->input('service_charge_annually', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'id' => 'service_charge_annually', 'name' => 'service_charge_annually', 'value' => $info->service_charge_annually]); ?>
                  </div>
                  <div class="form-group">
                    <label>Agent Fee (annually)</label>
                    <?= $this->Form->input('agent_fee_annually', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'name' => 'agent_fee_annually', 'id' => 'agent_fee_annually', 'value' => $info->agent_fee_annually]); ?>
                  </div>
                  <div class="form-group">
                    <label>Expected Annual Return of Investment Nett of Service Charge & Agent Free (%)</label>
                    <?= $this->Form->input('expected_annual_return', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'value' => $info->expected_annual_return]); ?>
                  </div>
                </div>
              </div> 
            </div>
            <div class="box-footer">
              <?= $this->Html->link(
                  '<i class="fa fa-chevron-left"></i><span> Back</span>', 
                  '/tabastaadmin/add-apt', 
                    [
                      'escape' => false,
                      'class' => 'btn btn-default'
                    ]
              ) ?>
              <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Save</button>
            </div>
            <?= $this->Form->end(); ?>
          </div>
        </div>
      </div>
    </section>
  </div>