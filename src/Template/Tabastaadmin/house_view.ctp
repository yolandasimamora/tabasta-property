  <?= $this->element('Common/admin_sidebar') ?> 
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        House Management | View
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="company.php">House Management</a></li>
        <li class="active">View</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default color-palette-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?= h($house->name) ?></h3>
            </div>
            <div class="box-body">
              <dl class="dl-horizontal">
                <dd>
                <?php if (!empty($house->property_image_files[0]['file_name'])):?>
                  <?php foreach($house->property_image_files as $img):?>
                  <?= $this->Html->image('logo/'.$img['file_name'],array('class'=>'media-‌​‌​object img img-thumbnail','alt'=>$img['file_name']) );?>
                  <?php endforeach;?>
                <?php endif;?>
                </dd>
                <dt>Property Name</dt>
                <dd><?= h($house->name) ?></dd>
                <dt>Province</dt>
                <dd><?php foreach($provinsi as $prov):?>
                <?= h($prov->name) ?>
                <?php endforeach;?></dd>
                <dt>City</dt>
                <dd><?php foreach($kota as $kota):?>
                <?= h($kota->name) ?>
                <?php endforeach;?></dd>
                <dt>Address</dt>
                <dd><?= h($house->address) ?>
                <dt>Tel</dt>
                <dd><?= h($house->telephone) ?></dd>
                <dt>Website</dt>
                <dd><?= $this->Html->link(
                          __($house->website), 
                          'http://'.$house->website.'/', 
                           array('class' => 'button', 'target' => '_blank')
                      );?></dd>
                </dd>
                <dt>Developer</dt>
                <dd><?= h($house->developer) ?></dd>
              </dl>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
         <div class="col-md-6">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Property Facilities</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <?php foreach($house->property_facilities_relations as $facilities):?>
                <?php $array = json_decode(json_encode($facilities), true);?>
                 <div class="col-sm-4 portfolio-list">
                  <div class="text-center">
                    <ul class="list-inline">
                      <li><i class="fa fa-tag"></i> <?php echo h($array['facility']['facility_name']);?></li>
                    </ul>
                  </div>
                </div> 
                <?php endforeach;?>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Property Area Information</h3>
            </div>
            <div class="box-body">
              <dl class="dl-horizontal">
                <dt>
                Public Transportation
                </dt>
                <dd>
                <?php echo h($house->property_area[0]['public_transportation']);?>
                </dd>
                <dt>
                Neighbourhood
                </dt>
                <dd>
                <?php echo h($house->property_area[0]['neighbourhood']);?>
                </dd>
                <dt>
                Industrial Area
                </dt>
                <dd>
                <?php echo h($house->property_area[0]['industrial_area']);?>
                </dd>
                <dt>
                Supermarket
                </dt>
                <dd>
                <?php echo h($house->property_area[0]['supermarket']);?>
                </dd>
              </dl>
            </div>
          </div>
        </div> 
      </div>
      <div class="row">
         <div class="col-md-6">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">House Certificates</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <?php foreach($house->property_certificates as $certificate):?>
                <?php $certi = json_decode(json_encode($certificate), true);?>
                 <div class="col-sm-4 portfolio-list">
                  <div class="text-center">
                    <ul class="list-inline">
                      <li><i class="fa fa-tag"></i> <?php echo h($certi['certificates_name']);?></li>
                    </ul>
                  </div>
                </div> 
                <?php endforeach;?>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Development Plan</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <?php foreach($house->property_development_plans as $dev_plans):?>
                <?php $dev_plan = json_decode(json_encode($dev_plans), true);?>
                 <div class="col-sm-4 portfolio-list">
                  <div class="text-center">
                    <ul class="list-inline">
                      <li><i class="fa fa-tag"></i> <?php echo h($dev_plan['dev_plan']);?></li>
                    </ul>
                  </div>
                </div> 
                <?php endforeach;?>
              </div>
            </div>
          </div>
        </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="box-footer">
              <div class="pull-left">
                <?= $this->Html->link(
                    '<i class="fa fa-chevron-left"></i><span> Back</span>', 
                    '/tabastaadmin/houses', 
                    [
                        'escape' => false,
                        'class' => 'btn btn-default'
                    ]
                ) ?>
              </div>
            </div>
          </div>
        </div>
    </section>
  </div>