  <?= $this->element('Common/admin_sidebar') ?> 
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Apartment Management | View
      </h1>
      <ol class="breadcrumb">
        <li><a href="index.php"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="company.php">Apartment Management</a></li>
        <li class="active">View</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default color-palette-box">
            <div class="box-header with-border">
              <h3 class="box-title"><?= h($apartment->name) ?></h3>
            </div>
            <div class="box-body">
              <dl class="dl-horizontal">
                <dd>
                <?php if (!empty($apartment->property_image_files[0]['file_name'])):?>
                  <?php foreach($apartment->property_image_files as $img):?>
                  <?= $this->Html->image('logo/'.$img['file_name'],array('class'=>'media-‌​‌​object img img-thumbnail','alt'=>$img['file_name']) );?>
                  <?php endforeach;?>
                <?php endif;?>
                </dd>
                <dt>Apartment Name</dt>
                <dd><?= h($apartment->name) ?></dd>
                <dt>Province</dt>
                <dd><?php foreach($provinsi as $prov):?>
                <?= h($prov->name) ?>
                <?php endforeach;?></dd>
                <dt>City</dt>
                <dd><?php foreach($kota as $kota):?>
                <?= h($kota->name) ?>
                <?php endforeach;?></dd>
                <dt>Address</dt>
                <dd><?= h($apartment->address) ?>
                <dt>Tel</dt>
                <dd><?= h($apartment->telephone) ?></dd>
                <dt>Website</dt>
                <dd><?= $this->Html->link(
                          __($apartment->website), 
                          'http://'.$apartment->website.'/', 
                           array('class' => 'button', 'target' => '_blank')
                      );?></dd>
                </dd>
                <dt>Developer</dt>
                <dd><?= h($apartment->developer) ?></dd>
              </dl>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
         <div class="col-md-12">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Apartment's Price Info By Room Type</h3>
            </div>
            <div class="box-body">
                <dl class="dl-horizontal">
                <dt>
                Studio 
                </dt>
                <dd>
                Selling Price Start From:   
                <b><?php echo $this->Number->format($apartment->property_apt_price_by_types[0]['studio'], ['places' => 2, 'before' => 'Rp. ']);?></b>
                </dd>
                <dd>
                Rent Prince Start From: 
                <b><?php echo $this->Number->format($apartment->property_apt_price_by_types[0]['rent_studio'], ['places' => 2, 'before' => 'Rp. ']);?></b>
                </dd>
                <dt>
                1 Bedroom 
                </dt>
                <dd>
                Selling Price Start From:
                <b><?php echo $this->Number->format($apartment->property_apt_price_by_types[0]['bedroom_1'], ['places' => 2, 'before' => 'Rp. ']);?></b>
                </dd>
                <dd>
                Rent Price Start From:
                <b><?php echo $this->Number->format($apartment->property_apt_price_by_types[0]['rent_bedroom_1'], ['places' => 2, 'before' => 'Rp. ']);?></b>
                </dd>
                <dt>
                2 Bedrooms 
                </dt>
                <dd>
                Selling Price Start From:
                <b><?php echo $this->Number->format($apartment->property_apt_price_by_types[0]['bedroom_2'], ['places' => 2, 'before' => 'Rp. ']);?></b>
                </dd>
                <dd>
                Rent Price Start From:
                <b><?php echo $this->Number->format($apartment->property_apt_price_by_types[0]['rent_bedroom_2'], ['places' => 2, 'before' => 'Rp. ']);?></b>
                </dd>
                <dt>
                Penthouse
                </dt>
                <dd>
                Selling Price Start From:
                <b><?php echo $this->Number->format($apartment->property_apt_price_by_types[0]['penthouse'], ['places' => 2, 'before' => 'Rp. ']);?></b>
                </dd>
                <dd>
                Rent Price Start From:
                <b><?php echo $this->Number->format($apartment->property_apt_price_by_types[0]['rent_penthouse'], ['places' => 2, 'before' => 'Rp. ']);?></b>
                </dd>
              </dl>
              <div class="row">
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
         <div class="col-md-6">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Apartment Facilities</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <?php foreach($apartment->property_facilities_relations as $facilities):?>
                <?php $array = json_decode(json_encode($facilities), true);?>
                 <div class="col-sm-4 portfolio-list">
                  <div class="text-center">
                    <ul class="list-inline">
                      <li><i class="fa fa-tag"></i> <?php echo h($array['facility']['facility_name']);?></li>
                    </ul>
                  </div>
                </div> 
                <?php endforeach;?>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Apartment Area Information</h3>
            </div>
            <div class="box-body">
              <dl class="dl-horizontal">
                <dt>
                Public Transportation
                </dt>
                <dd>
                <?php echo h($apartment->property_area[0]['public_transportation']);?>
                </dd>
                <dt>
                Neighbourhood
                </dt>
                <dd>
                <?php echo h($apartment->property_area[0]['neighbourhood']);?>
                </dd>
                <dt>
                Industrial Area
                </dt>
                <dd>
                <?php echo h($apartment->property_area[0]['industrial_area']);?>
                </dd>
                <dt>
                Supermarket
                </dt>
                <dd>
                <?php echo h($apartment->property_area[0]['supermarket']);?>
                </dd>
              </dl>
            </div>
          </div>
        </div> 
      </div>
      <div class="row">
         <div class="col-md-6">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Apartment Certificates</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <?php foreach($apartment->property_certificates as $certificate):?>
                <?php $certi = json_decode(json_encode($certificate), true);?>
                 <div class="col-sm-4 portfolio-list">
                  <div class="text-center">
                    <ul class="list-inline">
                      <li><i class="fa fa-tag"></i> <?php echo h($certi['certificates_name']);?></li>
                    </ul>
                  </div>
                </div> 
                <?php endforeach;?>
              </div>
            </div>
          </div>
          </div>
          <div class="col-md-6">
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Development Plan</h3>
                </div>
                <div class="box-body">
                    <dl class="dl-horizontal">
                      <dt>
                      Development Plan
                      </dt>
                      <dd>
                      <?php if(!empty($apartment->property_develpment_plans[0]->dev_plan)):?>
                        <?php echo h($apartment->property_develpment_plans[0]->dev_plan);?>
                      <?php endif;?>
                      </dd>
                      <dt>
                      Status
                      </dt>
                      <dd>
                      <?php if(!empty($apartment->property_develpment_plans[0]->status)):?>
                        <?php echo h($apartment->property_develpment_plans[0]->status);?>
                      <?php endif;?>
                      </dd>
                      <dt>
                      Due Date
                      </dt>
                      <dd>
                      <?php if(!empty($apartment->property_develpment_plans[0]->time)):?>
                        <?php echo h($apartment->property_develpment_plans[0]->time);?>
                      <?php endif;?>
                      </dd>
                    </dl>
                </div>
              </div>
          </div>
        </div>
        <div class="row">
           <div class="col-md-12">
            <div class="box box-success">
              <div class="box-header with-border">
                <h3 class="box-title">Investment Informations</h3>
              </div>
              <?php use Cake\I18n\Time;?> 
              <div class="box-body" style="margin:0 30px;">
                <div class="form-group">
                <label>Apartment Unit Type :</label>
                <?php echo h($apartment->property_investment_informations[0]['apt_unit']);?>
                </div>
                <div class="form-group">
                <label>Area :</label>
                <?php echo h($apartment->property_investment_informations[0]['area']);?>
                </div>
                <div class="form-group">
                  <label>Average Selling Price :</label>
                  <?php echo $this->Number->format($apartment->property_investment_informations[0]['avg_selling_price'], ['places' => 2, 'before' => 'Rp. ']);?>
                </div>
                <div class="form-group">
                  <label>Selling Price per m2 :</label>
                  <?php echo $this->Number->format($apartment->property_investment_informations[0]['selling_price_per_m2'], ['places' => 2, 'before' => 'Rp. ']);?>
                </div>
                <div class="form-group">
                  <label>Average Rental Price (monthly) :</label>
                  <?php echo $this->Number->format($apartment->property_investment_informations[0]['avg_rental_price_monthly'], ['places' => 2, 'before' => 'Rp. ']);?>
                </div>
                <div class="form-group">
                  <label>Average Rental Price (annually) :</label>
                  <?php echo $this->Number->format($apartment->property_investment_informations[0]['avg_rental_price_annually'], ['places' => 2, 'before' => 'Rp. ']);?>
                </div>
                <div class="form-group">
                  <label>Average Rental Price Annually (%) :</label>
                  <?php echo $this->Number->toPercentage($apartment->property_investment_informations[0]['avg_rental_price_annually_percentage']);?>
                </div>
                <div class="form-group">
                  <label>Service Charge (annually) :</label>
                  <?php echo $this->Number->format($apartment->property_investment_informations[0]['service_charge_annually'], ['places' => 2, 'before' => 'Rp. ']);?>
                </div>
                <div class="form-group">
                  <label>Agent Fee (annually) :</label>
                  <?php echo $this->Number->format($apartment->property_investment_informations[0]['agent_fee_anually'], ['places' => 2, 'before' => 'Rp. ']);?>
                </div>
                <div class="form-group">
                  <label>Expected Annual Return (%) :</label>
                  <?php echo $this->Number->toPercentage($apartment->property_investment_informations[0]['expected_annual_return']);?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="box-footer">
              <div class="pull-left">
                <?= $this->Html->link(
                    '<i class="fa fa-chevron-left"></i><span> Back</span>', 
                    '/tabastaadmin/apartments', 
                    [
                        'escape' => false,
                        'class' => 'btn btn-default'
                    ]
                ) ?>
              </div>
            </div>
          </div>
        </div>
    </section>
  </div>