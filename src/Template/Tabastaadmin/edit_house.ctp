 <?= $this->element('Common/admin_sidebar') ?>  
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        House Management | Edit House
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">House Management</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-success">
            <?= $this->Form->create('', ['name' => 'form_wizard', 'class' => 'contactform requestcertificate-form', 'enctype' => 'multipart/form-data']) ?>
              <div class="box-body">
              <div class="box-header with-border">
                  <h3 class="box-title">House Basic Information</h3>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Logo</label>
                          <br>
                          <?php if (!empty($houses->property_image_files)):?>
                          <?php foreach($houses->property_image_files as $files):?>
                            <?= $this->Html->image('logo/'.$files['file_name'],array('id' => 'imagePreview', 'class'=>'media-‌​‌​object img img-thumbnail','alt'=>$files['file_name']) );?>
                            <?= $this->Html->link('Delete', ['action' => 'deleteImageApt', $houses->id, $files['id']], ['class' => 'confirm_delete_logo']) ?>
                          <?php endforeach;?>
                          <?php else:?>
                            <div id="imagePreview" alt="" class="img-responsive"><img src="http://via.placeholder.com/250x250" alt="" class="img-responsive">
                            </div>
                          <?php endif;?>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Upload Image</label>
                            <?php  
                              echo $this->Form->input('logo', [
                                'onchange'=>'return fileValidation()',
                                'id' => 'file',
                                'class' => 'form-control',
                                'type' => 'file',
                                'label' => false,
                              ]);
                            ?>
                        </div>
                      </div>
                    </div>
                    <div id = 'dynamic_field_2'>
                      <button type="button" name="add_2" id="add_2" class="btn btn-success">Add More</button>
                    </div>
                    <div class="form-group">
                      <label>House Name</label><span style ="color:red;"> *</span>
                      <?= $this->Form->input('name', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'required' => true, 'value' => $houses->name]); ?>
                    </div>
                    <div class="form-group">
                      <label>Price</label><span style ="color:red;"> *</span>
                      <?= $this->Form->input('price', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'required' => true, 'value' => $houses->price]); ?>
                    </div>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Province</label><span style ="color:red;"> *</span>
                          <?= $this->Form->input('provinsi_id',array('type'=>'select','empty'=>'- Pilih Provinsi -','options'=>$provinsis,'id'=>'province_id','class'=>'form-control form-control-custom','onChange'=>'provChange()', 'label' => false, 'required' => true, 'value' => $houses->provinsi_id)); ?>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>City</label><span style ="color:red;"> *</span>
                          <?= $this->Form->input('kota_id',array('type'=>'select','empty'=>'- Pilih Kota -','options'=>$kota,'id'=>'city_id','class'=>'form-control form-control-custom', 'label' => false, 'required' => false, 'value' => $houses->kota_id)); ?>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <label>Address</label><span style ="color:red;"> *</span>
                      <?= $this->Form->input('address', ['class' => 'form-control', 'label' => false, 'type' => 'textarea', 'required' => true, 'value' => $houses->address]); ?>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Phone</label>
                      <?= $this->Form->input('telephone', ['class' => 'form-control', 'label' => false, 'type' => 'number', 'value' => $houses->telephone]); ?>
                    </div>
                    <div class="form-group">
                      <label>Website</label>
                      <?= $this->Form->input('website', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'value' => $houses->website]); ?>
                    </div>
                    <div class="form-group">
                      <label>House Facility</label>
                        <ul class="cat-list">
                        <?php 
                          foreach ($facilities as $key => $facility) {
                            echo '<li><label>' . $this->Form->checkbox('facility.' . $key, ['value' => $key, 'label' => false, 'hiddenField' => false, 'class' => 'multi-check', 'checked' => (in_array($key, $selectedCat)) ? true:false]) . ' ' . h($facility) . '</label></li>';
                          }
                        ?>
                        </ul>
                    </div>
                    <div class="form-group">
                      <label>Developer</label><span style ="color:red;"> *</span>
                      <?= $this->Form->input('developer', ['class' => 'form-control summernote', 'label' => false, 'type' => 'text', 'required' => true, 'value' => $houses->developer]); ?>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="box-header with-border">
                  <h3 class="box-title">House Area Information</h3>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <label>Public Transportation</label><span style ="color:red;"> *</span>
                      <?= $this->Form->input('public_transportation', ['class' => 'form-control', 'label' => false, 'type' => 'textarea', 'value' => $houses->property_area[0]['public_transportation']]); ?>
                    </div>
                    <div class="col-md-6">
                      <label>Neighbourhood</label><span style ="color:red;"> *</span>
                      <?= $this->Form->input('neighbourhood', ['class' => 'form-control', 'label' => false, 'type' => 'textarea', 'value' => $houses->property_area[0]['neighbourhood']]); ?>
                    </div>
                  </div>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <label>Industrial Area</label><span style ="color:red;"> *</span>
                      <?= $this->Form->input('industrial_area', ['class' => 'form-control', 'label' => false, 'type' => 'textarea', 'value' => $houses->property_area[0]['industrial_area']]); ?>
                    </div>
                    <div class="col-md-6">
                      <label>Supermarket</label><span style ="color:red;"> *</span>
                      <?= $this->Form->input('supermarket', ['class' => 'form-control', 'label' => false, 'type' => 'textarea', 'value' => $houses->property_area[0]['supermarket']]); ?>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="box-header with-border">
                  <h3 class="box-title">Certificates</h3>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <div id = 'dynamic_field'>
                      <label></label>
                      <?= $this->Form->input('certificate_name', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'name' => 'certificate_name[]']); ?>
                      </div>
                      <button type="button" name="add" id="add" class="btn btn-success">Add More</button>
                    </div>
                    <div class="col-md-6">
                    <label>Registered Certificates</label>
                    <?php foreach($houses->property_certificates as $certi):?>
                      <ul class="cat-list">
                        <li><?php echo h($certi->certificates_name);?>
                          <?= $this->Html->link('Delete', ['action' => 'deleteCertificatesHouse', $certi->id, $houses->id], ['class' => 'confirm_delete_logo']) ?>
                        </li>
                      </ul>
                    <?php endforeach;?>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="box-header with-border">
                  <h3 class="box-title">Development Plan</h3>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <div id = 'dynamic_field_1'>
                        <label></label>
                        <?= $this->Form->input('dev_plan', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'name' => 'dev_plan[]']); ?>
                      </div>
                      <button type="button" name="add_1" id="add_1" class="btn btn-success">Add More</button>
                    </div>
                    <div class="col-md-6">
                    <label>Registered Certificates</label>
                     <?php foreach($houses->property_development_plans as $dev):?>
                      <ul class="cat-list">
                        <li><?php echo h($dev->dev_plan);?>
                          <?= $this->Html->link('Delete', ['action' => 'deleveDevPlanHouse', $dev->id, $houses->id], ['class' => 'confirm_delete_logo']) ?>
                        </li>
                      </ul>
                    <?php endforeach;?>
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-body">
                <div class="box-header with-border">
                  <h3 class="box-title">Financial Information</h3>
                </div>
                <div class="form-group">
                  <div class="row">
                    <div class="col-md-6">
                      <label></label>
                      <?= $this->Form->input('financial_information', ['class' => 'form-control', 'label' => false, 'type' => 'text']); ?>
                    </div>
                    <div class="col-md-6">
                    </div>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <a href="company.php" class="btn btn-default"><i class="fa fa-chevron-left"></i> Back</a>
                <button type="submit" class="btn btn-success pull-right" id="button"><i class="fa fa-save"></i> Save</button>
                <?= $this->Form->end(); ?>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
  </div>