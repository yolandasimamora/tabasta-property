  <?= $this->element('Common/admin_sidebar') ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Facilities Management | Add New Facility
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Facilities Management</a></li>
        <li class="active">Add New</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default color-palette-box">
            <div class="box-header with-border">
              <h3 class="box-title">Add New Facility</h3>
            </div>
            <?= $this->Form->create('', ['name' => 'form_wizard', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) ?>
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Facility Name</label>
                  <div class="col-sm-6">
                    <?= $this->Form->input('facility_name', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'required' => true]); ?>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <?= $this->Html->link(
                              '<i class="fa fa-chevron-left"></i><span> Back</span>', 
                              '/kazokkuadmin/category/', 
                              [
                                  'escape' => false,
                                  'class' => 'btn btn-default'
                              ]
                          ) ?>
                <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Save</button>
              </div>
            <?= $this->Form->end(); ?>
          </div>
        </div>
      </div>
    </section>
  </div>