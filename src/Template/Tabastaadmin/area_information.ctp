  <?= $this->element('Common/admin_sidebar') ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Area Information Management
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Area Information Management</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default color-palette-box">
            <div class="box-header with-border">
              <h3 class="box-title">Area Information List</h3>
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-12">
                  <ul class="nav nav-tabs">
                      <li class="active"><a href="#tab-parent1" data-toggle="tab">Public Transportation</a></li>
                      <li><a href="#tab-parent2" data-toggle="tab">Neigbourhood</a></li>
                      <li><a href="#tab-parent3" data-toggle="tab">Industrial Area</a></li>
                      <li><a href="#tab-parent4" data-toggle="tab">Supermarket</a></li>
                  </ul>
                  <div class="tab-content">
                   <div class="tab-pane active" id="tab-parent1">
                     <div class="box box-default color-palette-box">
                     <div class="box-header with-border">
                        <h3 class="box-title">Public Transportation List</h3>
                        </div>
                        <div class="box-body">
                          <div class="row">
                            <div class="col-md-12">
                              <?= $this->Html->link(
                                '<i class="fa fa-plus-circle"></i><span> Add New</span>', 
                                '/tabastaadmin/add-transportation', 
                                [
                                    'escape' => false,
                                    'class' => 'btn btn-sm btn-success',
                                    'style' => 'margin-bottom:20px;'
                                ]
                            ) ?>
                               
                             
                              <table id="table" class="table table-bordered table-hover dataTable">
                                  <thead>
                                    <tr>
                                      <th>ID</th>
                                      <th>Public Transportation</th>
                                      <th>Actions</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  <?php foreach($transport as $tp):?>
                                    <tr>
                                      <td><?= h($tp->id) ?></td>
                                      <td><?= h($tp->name) ?></td>
                                      <td>
                                        <div class="btn-group">
                                          <?= $this->Html->link(
                                          '<i class="fa fa-edit"></i><span> Edit</span>', 
                                          '/tabastaadmin/transport-edit/'.$tp->id, 
                                          [
                                              'escape' => false,
                                              'class' => 'btn btn-xs btn-default'
                                          ]
                                          ) ?>
                                          <?= $this->Form->postLink(__('<i class="fa fa-trash"></i> Delete'), ['action' => 'transportDelete', $tp->id], ['confirm' => __('Are you sure you want to delete # {0}?', $tp->id), 'escape' =>false, 'class' => 'btn btn-xs btn-danger']) ?>
                                        </div>
                                      </td>
                                    </tr>
                                  <?php endforeach;?>
                                  </tbody>
                                </table>
                            </div>
                          </div>
                        </div>
                      </div>
                   </div>
                   <div class="tab-pane" id="tab-parent2">
                     <div class="box box-default color-palette-box">
                     <div class="box-header with-border">
                      <h3 class="box-title">Webdev Category List</h3>
                        </div>
                        <div class="box-body">
                          <div class="row">
                            <div class="col-md-12">
                              <?= $this->Html->link(
                                '<i class="fa fa-plus-circle"></i><span> Add New</span>', 
                                '/kazokkuadmin/add-webdev-category', 
                                [
                                    'escape' => false,
                                    'class' => 'btn btn-sm btn-success',
                                    'style' => 'margin-bottom:20px;'
                                ]
                            ) ?>
                               
                             
                              <table id="table" class="table table-bordered table-hover dataTable">
                                  <thead>
                                    <tr>
                                      <th>ID</th>
                                      <th>Name</th>
                                      <th>Webdev Count</th>
                                      <th>Actions</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  <?php foreach($webdev_category as $category):?>
                                    <tr>
                                      <td><?= h($category->id) ?></td>
                                      <td><?= h($category->name) ?></td>
                                      <td><?php 
                                        
                                      ?></td>
                                      <td>
                                        <div class="btn-group">
                                          <?= $this->Html->link(
                                          '<i class="fa fa-edit"></i><span> Edit</span>', 
                                          '/kazokkuadmin/webdev-category-edit/'.$category->id, 
                                          [
                                              'escape' => false,
                                              'class' => 'btn btn-xs btn-default'
                                          ]
                                          ) ?>
                                          <?= $this->Form->postLink(__('<i class="fa fa-trash"></i> Delete'), ['action' => 'deleteWebdevCategory', $category->id], ['confirm' => __('Are you sure you want to delete # {0}?', $category->id), 'escape' =>false, 'class' => 'btn btn-xs btn-danger']) ?>
                                        </div>
                                      </td>
                                    </tr>
                                  <?php endforeach;?>
                                  </tbody>
                                </table>
                            </div>
                          </div>
                        </div>
                      </div>
                   </div>
                   <div class="tab-pane" id="tab-parent3">
                     <div class="box box-default color-palette-box">
                     <div class="box-header with-border">
                      <h3 class="box-title">Webdev Category List</h3>
                        </div>
                        <div class="box-body">
                          <div class="row">
                            <div class="col-md-12">
                              <?= $this->Html->link(
                                '<i class="fa fa-plus-circle"></i><span> Add New</span>', 
                                '/kazokkuadmin/add-webdev-category', 
                                [
                                    'escape' => false,
                                    'class' => 'btn btn-sm btn-success',
                                    'style' => 'margin-bottom:20px;'
                                ]
                            ) ?>
                               
                             
                              <table id="table" class="table table-bordered table-hover dataTable">
                                  <thead>
                                    <tr>
                                      <th>ID</th>
                                      <th>Name</th>
                                      <th>Webdev Count</th>
                                      <th>Actions</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  <?php foreach($webdev_category as $category):?>
                                    <tr>
                                      <td><?= h($category->id) ?></td>
                                      <td><?= h($category->name) ?></td>
                                      <td><?php 
                                        
                                      ?></td>
                                      <td>
                                        <div class="btn-group">
                                          <?= $this->Html->link(
                                          '<i class="fa fa-edit"></i><span> Edit</span>', 
                                          '/kazokkuadmin/webdev-category-edit/'.$category->id, 
                                          [
                                              'escape' => false,
                                              'class' => 'btn btn-xs btn-default'
                                          ]
                                          ) ?>
                                          <?= $this->Form->postLink(__('<i class="fa fa-trash"></i> Delete'), ['action' => 'deleteWebdevCategory', $category->id], ['confirm' => __('Are you sure you want to delete # {0}?', $category->id), 'escape' =>false, 'class' => 'btn btn-xs btn-danger']) ?>
                                        </div>
                                      </td>
                                    </tr>
                                  <?php endforeach;?>
                                  </tbody>
                                </table>
                            </div>
                          </div>
                        </div>
                      </div>
                   </div>
                   <div class="tab-pane" id="tab-parent4">
                     <div class="box box-default color-palette-box">
                     <div class="box-header with-border">
                      <h3 class="box-title">Webdev Category List</h3>
                        </div>
                        <div class="box-body">
                          <div class="row">
                            <div class="col-md-12">
                              <?= $this->Html->link(
                                '<i class="fa fa-plus-circle"></i><span> Add New</span>', 
                                '/kazokkuadmin/add-webdev-category', 
                                [
                                    'escape' => false,
                                    'class' => 'btn btn-sm btn-success',
                                    'style' => 'margin-bottom:20px;'
                                ]
                            ) ?>
                               
                             
                              <table id="table" class="table table-bordered table-hover dataTable">
                                  <thead>
                                    <tr>
                                      <th>ID</th>
                                      <th>Name</th>
                                      <th>Webdev Count</th>
                                      <th>Actions</th>
                                    </tr>
                                  </thead>
                                  <tbody>
                                  <?php foreach($webdev_category as $category):?>
                                    <tr>
                                      <td><?= h($category->id) ?></td>
                                      <td><?= h($category->name) ?></td>
                                      <td><?php 
                                        
                                      ?></td>
                                      <td>
                                        <div class="btn-group">
                                          <?= $this->Html->link(
                                          '<i class="fa fa-edit"></i><span> Edit</span>', 
                                          '/kazokkuadmin/webdev-category-edit/'.$category->id, 
                                          [
                                              'escape' => false,
                                              'class' => 'btn btn-xs btn-default'
                                          ]
                                          ) ?>
                                          <?= $this->Form->postLink(__('<i class="fa fa-trash"></i> Delete'), ['action' => 'deleteWebdevCategory', $category->id], ['confirm' => __('Are you sure you want to delete # {0}?', $category->id), 'escape' =>false, 'class' => 'btn btn-xs btn-danger']) ?>
                                        </div>
                                      </td>
                                    </tr>
                                  <?php endforeach;?>
                                  </tbody>
                                </table>
                            </div>
                          </div>
                        </div>
                      </div>
                   </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  </div>