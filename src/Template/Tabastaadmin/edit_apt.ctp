 <?= $this->element('Common/admin_sidebar') ?>  
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Apartment Management | Edit Apartment
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Apartment Management</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box" style="border-top:0;">
            <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Edit Apartment Detail</a></li>
              <li><a href="#tab_2" data-toggle="tab">Edit Property Investment Information Detail </a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <?= $this->Form->create('', ['name' => 'form_wizard', 'class' => 'contactform requestcertificate-form', 'enctype' => 'multipart/form-data']) ?>
                  <div class="box-body">
                    <div class="row">
                      <div class="col-md-6">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Logo</label>
                              <br>
                              <?php if (!empty($apartments->property_image_files)):?>
                              <?php foreach($apartments->property_image_files as $files):?>
                                <?= $this->Html->image('logo/'.$files['file_name'],array('id' => 'imagePreview', 'class'=>'media-‌​‌​object img img-thumbnail','alt'=>$files['file_name']) );?>
                                <?= $this->Html->link('Delete', ['action' => 'deleteImageApt', $apartments->id, $files['id']], ['class' => 'confirm_delete_logo']) ?>
                              <?php endforeach;?>
                              <?php else:?>
                                <div id="imagePreview" alt="" class="img-responsive"><img src="http://via.placeholder.com/250x250" alt="" class="img-responsive">
                                </div>
                              <?php endif;?>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Upload Image</label>
                              <?php  
                                  echo $this->Form->input('logo', [
                                      'onchange'=>'return fileValidation()',
                                      'id' => 'file',
                                      'class' => 'form-control',
                                      'type' => 'file',
                                      'label' => false,
                                  ]);
                              ?>
                            </div>
                          </div>
                        </div>
                        <div id = 'dynamic_field_2'>
                          <button type="button" name="add_2" id="add_2" class="btn btn-success">Add More</button>
                        </div>
                        <div class="form-group">
                          <label>Apartment Name</label><span style ="color:red;"> *</span>
                          <?= $this->Form->input('name', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'required' => true, 'value' => $apartments->name]); ?>
                        </div>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>Province</label><span style ="color:red;"> *</span>
                              <?= $this->Form->input('provinsi_id',array('type'=>'select','empty'=>'- Pilih Provinsi -','options'=>$provinsis,'id'=>'province_id','class'=>'form-control form-control-custom','onChange'=>'provChange()', 'label' => false, 'required' => true, 'value' => $apartments->provinsi_id)); ?>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="form-group">
                              <label>City</label><span style ="color:red;"> *</span>
                              <?= $this->Form->input('kota_id',array('type'=>'select','empty'=>'- Pilih Kota -','options'=>$kota,'id'=>'city_id','class'=>'form-control form-control-custom', 'label' => false, 'required' => false, 'value' => $apartments->kota_id)); ?>
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                          <label>Address</label><span style ="color:red;"> *</span>
                          <?= $this->Form->input('address', ['class' => 'form-control', 'label' => false, 'type' => 'textarea', 'required' => true, 'value' => $apartments->address]); ?>
                        </div>
                        <label>Price (Rp.)</label><span style ="color:red;"> *</span>
                        <div class="row">
                          <div class="col-md-4">
                            <div style ="font-weight: bold;text-align: center;">Studio</div>
                          </div>
                          <div class="col-md-4">
                            Selling Price Start From
                            <?= $this->Form->input('studio', ['class' => 'form-control', 'label' => false, 'type' => 'number', 'value' => $apartments->property_apt_price_by_types[0]->studio]); ?>
                          </div>
                          <div class="col-md-4">
                            Rental Price Start From
                            <?= $this->Form->input('rent_studio', ['class' => 'form-control', 'label' => false, 'type' => 'number', 'value' => $apartments->property_apt_price_by_types[0]->rent_studio]); ?>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-4">
                            <div style ="font-weight: bold;text-align: center;">1 Bedroom</div>
                          </div>
                          <div class="col-md-4">
                            Selling Price Start From
                            <?= $this->Form->input('bedroom_1', ['class' => 'form-control', 'label' => false, 'type' => 'number', 'required' => true, 'value' => $apartments->property_apt_price_by_types[0]->bedroom_1]); ?>
                          </div>
                          <div class="col-md-4">
                            Rental Price Start From
                            <?= $this->Form->input('rent_bedroom_1', ['class' => 'form-control', 'label' => false, 'type' => 'number']); ?>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-4">
                            <div style ="font-weight: bold;text-align: center;">2 Bedrooms</div>
                          </div>
                          <div class="col-md-4">
                            Selling Price Start From
                            <?= $this->Form->input('bedroom_2', ['class' => 'form-control', 'label' => false, 'type' => 'number', 'value' => $apartments->property_apt_price_by_types[0]->bedroom_2]); ?>
                          </div>
                          <div class="col-md-4">
                            Rental Price Start From
                            <?= $this->Form->input('rent_bedroom_2', ['class' => 'form-control', 'label' => false, 'type' => 'number', 'value' => $apartments->property_apt_price_by_types[0]->rent_bedroom_2]); ?>
                          </div>
                        </div>
                        <div class="row">
                          <div class="col-md-4">
                            <div style ="font-weight: bold;text-align: center;">Penthouse</div>
                          </div>
                          <div class="col-md-4">
                            Selling Price Start From
                            <?= $this->Form->input('penthouse', ['class' => 'form-control', 'label' => false, 'type' => 'number', 'required' => true, 'value' => $apartments->property_apt_price_by_types[0]->penthouse]); ?>
                          </div>
                          <div class="col-md-4">
                            Rental Price Start From
                            <?= $this->Form->input('rent_penthouse', ['class' => 'form-control', 'label' => false, 'type' => 'number', 'required' => true, 'value' => $apartments->property_apt_price_by_types[0]->rent_penthouse]); ?>
                          </div>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label>Phone</label>
                          <?= $this->Form->input('telephone', ['class' => 'form-control', 'label' => false, 'type' => 'number', 'value' => $apartments->telephone]); ?>
                        </div>
                        <div class="form-group">
                          <label>Website</label>
                          <?= $this->Form->input('website', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'value' => $apartments->website]); ?>
                        </div>
                        <div class="row">
                          <div class="col-md-12">
                            <div class="form-group">
                              <label>Apartment Facilities</label>
                              <ul class="cat-list">
                                <?php 
                                  foreach ($facilities as $key => $facility) {
                                    echo '<li><label>' . $this->Form->checkbox('facility.' . $key, ['value' => $key, 'label' => false, 'hiddenField' => false, 'class' => 'multi-check', 'checked' => (in_array($key, $selectedCat)) ? true:false]) . ' ' . h($facility) . '</label></li>';
                                  }
                                ?>
                              </ul>
                            </div>
                          </div>
                          <div class="col-md-6">
                          </div>
                        </div>
                        <div class="form-group">
                          <label>Developer</label><span style ="color:red;"> *</span>
                          <?= $this->Form->input('developer', ['class' => 'form-control summernote', 'label' => false, 'type' => 'text', 'required' => true, 'value' => $apartments->developer]); ?>

                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="box-header with-border">
                      <h3 class="box-title">House Area Information</h3>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-6">
                          <label>Public Transportation</label><span style ="color:red;"> *</span>
                          <?= $this->Form->input('public_transportation', ['class' => 'form-control', 'label' => false, 'type' => 'textarea', 'value' => $apartments->property_area[0]['public_transportation']]); ?>
                        </div>
                        <div class="col-md-6">
                          <label>Neighbourhood</label><span style ="color:red;"> *</span>
                          <?= $this->Form->input('neighbourhood', ['class' => 'form-control', 'label' => false, 'type' => 'textarea', 'value' => $apartments->property_area[0]['neighbourhood']]); ?>
                        </div>
                      </div>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-6">
                          <label>Industrial Area</label><span style ="color:red;"> *</span>
                          <?= $this->Form->input('industrial_area', ['class' => 'form-control', 'label' => false, 'type' => 'textarea', 'value' => $apartments->property_area[0]['industrial_area']]); ?>
                        </div>
                        <div class="col-md-6">
                          <label>Supermarket</label><span style ="color:red;"> *</span>
                          <?= $this->Form->input('supermarket', ['class' => 'form-control', 'label' => false, 'type' => 'textarea', 'value' => $apartments->property_area[0]['supermarket']]); ?>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="box-header with-border">
                      <h3 class="box-title">Certificates</h3>
                    </div>
                    <div class="form-group">
                      <div class="row">
                        <div class="col-md-6">
                          <div id = 'dynamic_field'>
                          <label></label>
                          <?= $this->Form->input('certificate_name', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'name' => 'certificate_name[]']); ?>
                          </div>
                          <button type="button" name="add" id="add" class="btn btn-success">Add More</button>
                          <br>
                          <br>
                          <label>Registered Certificates</label>
                            <?php foreach($apartments->property_certificates as $certi):?>
                            <ul class="cat-list">
                              <li><?php echo h($certi->certificates_name);?>
                                <?= $this->Html->link('Delete', ['action' => 'deleteCertificatesApt', $certi->id, $apartments->id], ['class' => 'confirm_delete_logo']) ?>
                              </li>
                            </ul>
                        <?php endforeach;?>
                        </div>
                        <div class="col-md-6">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="box-body">
                    <div class="box-header with-border">
                      <h3 class="box-title">Development Plan</h3>
                    </div>
                    <div class="form-group">
                      <div id = 'dynamic_field_1'>
                        <div class="row">
                          <div class="col-md-6">
                            <label>Development's Detail</label>
                            <?= $this->Form->input('dev_plan', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'name' => 'dev_plan', 'value' => $apartments->property_development_plans[0]->dev_plan]); ?>
                          </div>
                          <div class="col-md-3">
                            <label>Status</label>
                            <?php echo $this->Form->select('status[]', [
                                    'Planning' => 'Planning',
                                    'Construction' => 'Construction',
                                    'Completed' => 'Completed'
                                ], ['class'=>'form-control form-control-custom', 'name' => 'status', 'value' => $apartments->property_development_plans[0]->status]);?>
                          </div>
                          <div class="col-sm-3">
                            <label>Due Date</label>
                            <div class="input-group date">
                              <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                              </div>
                              <?= $this->Form->input('time', ['class' => 'form-control datepicker', 'label' => false, 'id' => 'datetimepicker', 'value' => $apartments->property_development_plans[0]->time]);  ?>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="box-footer">
                    <?= $this->Html->link(
                          '<i class="fa fa-chevron-left"></i><span> Back</span>', 
                          '/tabastaadmin/apartments', 
                            [
                              'escape' => false,
                              'class' => 'btn btn-default'
                            ]
                    ) ?>
                    <button type="submit" class="btn btn-success pull-right" id="button"><i class="fa fa-save"></i> Save</button>
                    <?= $this->Form->end(); ?>
                  </div>
              </div>
              <div class="tab-pane" id="tab_2">
                <?= $this->Html->link(
                  '<i class="fa fa-plus-circle"></i><span> Add New</span>', 
                  '/tabastaadmin/add_investment_information/'.$apartments->id, 
                    [
                      'escape' => false,
                      'class' => 'btn btn-sm btn-success',
                      'style' => 'margin-bottom:20px;'
                    ]
                ) ?>
                <table id="table" class="table table-bordered table-hover dataTable">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>Property Name</th>
                      <th>Property Unit</th>
                      <th>Area</th>
                      <th>Avg. Selling Price</th>
                      <th>Expected Annual Return</th>
                      <th width="150px">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($apartments->property_investment_informations as $info):?>
                    <tr>
                      <td><?= h($info->id) ?></td>
                      <td><?= h($info->property->name) ?></td>
                      <td><?= h($info->apt_unit) ?></td>
                      <td><?= h($info->area) ?></td>
                      <td><?= h($info->avg_selling_price) ?></td>
                      <td><?php echo $this->Number->toPercentage($info->expected_annual_return);?></td>
                      <td>
                        <div class="btn-group">
                          <?= $this->Html->link(
                              '<i class="fa fa-eye"></i><span> View</span>', 
                              '/tabastaadmin/view-investment-information/'.$info->id, 
                              [
                                  'escape' => false,
                                  'class' => 'btn btn-xs btn-default'
                              ]
                          ) ?>
                          <?= $this->Html->link(
                              '<i class="fa fa-edit"></i><span> Edit</span>', 
                              '/tabastaadmin/edit-investment-information/'.$info->id, 
                              [
                                  'escape' => false,
                                  'class' => 'btn btn-xs btn-default'
                              ]
                          ) ?>

                          <?= $this->Form->postLink(__('<i class="fa fa-trash"></i> Delete'), ['action' => 'deleteInvestmentInformation', $info->id, $apartments->id], ['confirm' => __('Are you sure you want to delete # {0}?', $info->id), 'escape' =>false, 'class' => 'btn btn-xs btn-danger']) ?>
                        </div>
                      </td>
                    </tr>
                    <?php endforeach;?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
    </section>
  </div>