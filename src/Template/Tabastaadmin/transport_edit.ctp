  <?= $this->element('Common/admin_sidebar') ?>
  <div class="content-wrapper">
    <section class="content-header">
      <h1>
        Property Area Management | Edit Transportation Information
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Property Area Management</a></li>
        <li class="active">Edit</li>
      </ol>
    </section>

    <section class="content">
      <div class="row">
        <div class="col-md-12">
          <div class="box box-default color-palette-box">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Transportation Information</h3>
            </div>
            <?= $this->Form->create('', ['name' => 'form_wizard', 'class' => 'form-horizontal', 'enctype' => 'multipart/form-data']) ?>
              <div class="box-body">
                <div class="form-group">
                  <label for="" class="col-sm-2 control-label">Transportation Name</label>
                  <div class="col-sm-6">
                    <?= $this->Form->input('name', ['class' => 'form-control', 'label' => false, 'type' => 'text', 'required' => true, 'value' => $transportation->name]); ?>
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <?= $this->Html->link(
                              '<i class="fa fa-chevron-left"></i><span> Back</span>', 
                              '/tabastaadmin/areaInformation/', 
                              [
                                  'escape' => false,
                                  'class' => 'btn btn-default'
                              ]
                          ) ?>
                <button type="submit" class="btn btn-success pull-right"><i class="fa fa-save"></i> Save</button>
              </div>
            <?= $this->Form->end(); ?>
          </div>
        </div>
      </div>
    </section>
  </div>