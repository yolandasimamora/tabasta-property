<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Tabasta Property | ';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tabasta Property</title>
    <?= $this->Html->meta('icon') ?>

    <?= $this->Html->css('cake.css') ?>

    <!-- Bootstrap Core CSS -->
    <?= $this->Html->css('bootstrap.css') ?>
    <?= $this->Html->css('cake.css') ?>
    <?= $this->Html->css('creative.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>

</head>
<body>
    <?= $this->Flash->render() ?>
    
    <?= $this->element('Common/client_header') ?>
    <?= $this->fetch('content') ?>
    <?= $this->element('Common/client_footer') ?>

<?php use Cake\Routing\Router; ?> 
<script>
function provChange(){
  var province = $("#province_id").val();
  $.ajax({
    url: "<?php echo Router::url(array("controller" => "Client", "action" => "ajax_city_id")); ?>/"+province+"/<?php //echo $this->request->data('Car.city_id'); ?>",cache: false,
    success: function(msg){$("#city_id").html(msg); },
    "statusCode": {
      403: function() {
        window.location.href="<?php //echo $this->Html->url(array('controller'=>'front','action'=>'index')); ?>"
      },
      500: function() {
        alert('Error Server Side occured');
      }
    }
  });
}
</script>
<script src="<?php echo $this->Url->script('jquery.min.js');?>"></script>
<script src="<?php echo $this->Url->script('bootstrap.min.js');?>"></script>
<script src="<?php echo $this->Url->script('bootstrap-datepicker.min.js');?>"></script>


<script>
    $(".background-message").delay(2000).fadeOut(200, function() {
    $(this).alert('close'); 
});
</script>
</body>
</html>
