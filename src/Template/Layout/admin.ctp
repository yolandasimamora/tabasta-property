<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Tabasta Property Admin | ';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        <?= $cakeDescription ?> 
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="\">
    <meta name="keywords" content="\">
    <meta name="author" content="\">
    <?php header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
    header("Pragma: no-cache"); // HTTP 1.0.
    header("Expires: 0");?>

    <?= $this->Html->css('bootstrap.min.css') ?>
    <?= $this->Html->css('AdminLTE.min.css') ?>
    <?= $this->Html->css('skins/_all-skins.min.css') ?>
    <?= $this->Html->css('main.css') ?>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
    <?= $this->Flash->render() ?>
        <?= $this->element('Common/admin_header') ?>
        <?= $this->fetch('content') ?>


        <?= $this->element('Common/admin_footer') ?>

<script type="text/javascript">
function fileValidation(){
    var fileInput = document.getElementById('file');
    var filePath = fileInput.value;
    var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;
    if(!allowedExtensions.exec(filePath)){
        alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
        fileInput.value = '';
        return false;
    }else{
        //Image preview
        if (fileInput.files && fileInput.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                document.getElementById('imagePreview').innerHTML = '<img src="'+e.target.result+'" class = "img-responsive"/>';
            };
            reader.readAsDataURL(fileInput.files[0]);
        } 
    }
}
</script> 

<?php use Cake\Routing\Router; ?> 
<script>
function provChange(){
  var province = $("#province_id").val();
  $.ajax({
    url: "<?php echo Router::url(array("controller" => "Tabastaadmin", "action" => "ajax_city_id")); ?>/"+province+"/<?php //echo $this->request->data('Car.city_id'); ?>",cache: false,
    success: function(msg){$("#city_id").html(msg); },
    "statusCode": {
      403: function() {
        window.location.href="<?php //echo $this->Html->url(array('controller'=>'front','action'=>'index')); ?>"
      },
      500: function() {
        alert('Error Server Side occured');
      }
    }
  });
}
</script>

 <script>
   var url = document.location.toString();
   if (url.match('#')) {
       $('.nav-tabs a[href="#' + url.split('#')[1] + '"]').tab('show');
   } 
</script>

 <script>  
 $(document).ready(function(){  
      var i=1;  
      $('#add').click(function(){  
           i++;  
           $('#dynamic_field').append('<tr id="row'+i+'"><td><input type="text" name="certificate_name[]"  class="form-control" style="width:448px;" /></td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove" style = "margin-bottom: 30px;margin-left: 5px;">X</button></td></tr>');  
    });  
      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
      $('#submit').click(function(){            
           $.ajax({  
                url:"name.php",  
                method:"POST",  
                data:$('#add_name').serialize(),  
                success:function(data)  
                {  
                     alert(data);  
                     $('#add_name')[0].reset();  
                }  
           });  
      }); 

 });  
 </script>
 <script>  
 $(document).ready(function(){  
      var a=100;  
      $('#add_1').click(function(){  
           a++;  
           $('#dynamic_field_1').append('<tr id="row'+a+'"><td><input type="text" name="dev_plan[]"  class="form-control" style="width:448px;" /></td><td><input name="time[]"  class="form-control datepicker" id="datetimepicker" style="width:448px;" /></td><td><button type="button" name="remove" id="'+a+'" class="btn btn-danger btn_remove_1" style = "margin-bottom: 30px;margin-left: 5px;">X</button></td></tr>');  
    });  
      $(document).on('click', '.btn_remove_1', function(){  
           var button_id_1 = $(this).attr("id");   
           $('#row'+button_id_1+'').remove();  
      });  
      $('#submit').click(function(){            
           $.ajax({  
                url:"name.php",  
                method:"POST",  
                data:$('#add_name_1').serialize(),  
                success:function(data)  
                {  
                     alert(data);  
                     $('#add_name_1')[0].reset();  
                }  
           });  
      }); 

 });  
 </script>

<script>  
 $(document).ready(function(){  
      var a=100;  
      $('#add_2').click(function(){  
           a++;  
           $('#dynamic_field_2').append('<tr id="row'+a+'"><td><input type="file" name="file_name[]"  class="form-control" style="width:448px;" /></td><td><button type="button" name="remove" id="'+a+'" class="btn btn-danger btn_remove_2" style = "margin-bottom: 30px;margin-left: 5px;">X</button></td></tr>');  
    });  
      $(document).on('click', '.btn_remove_2', function(){  
           var button_id_2 = $(this).attr("id");   
           $('#row'+button_id_2+'').remove();  
      });  
      $('#submit').click(function(){            
           $.ajax({  
                url:"name.php",  
                method:"POST",  
                data:$('#add_name_2').serialize(),  
                success:function(data)  
                {  
                     alert(data);  
                     $('#add_name_2')[0].reset();  
                }  
           });  
      }); 

 });  
 </script>

 <script>  
 $(document).ready(function(){  
      var a=100;  
      $('#add_3').click(function(){  
           a++;  
           $('#dynamic_field_3').append('<tr id="row'+a+'"><td><input type="text" name="apt_unit" class="form-control" id="apt_unit"/></div></td><td><button type="button" name="remove" id="'+a+'" class="btn btn-danger btn_remove_2" style = "margin-bottom: 30px;margin-left: 5px;">X</button></td></tr>');  
    });  
      $(document).on('click', '.btn_remove_3', function(){  
           var button_id_2 = $(this).attr("id");   
           $('#row'+button_id_2+'').remove();  
      });  
      $('#submit').click(function(){            
           $.ajax({  
                url:"name.php",  
                method:"POST",  
                data:$('#add_name_3').serialize(),  
                success:function(data)  
                {  
                     alert(data);  
                     $('#add_name_3')[0].reset();  
                }  
           });  
      }); 

 });  
 </script>
 <script type="text/javascript">
   $('.datepicker').datepicker({
      autoclose: true
    });
</script>
<script type="text/javascript">
  $(document).ready(function(e) {
    $("input").change(function() {
      var area = parseFloat($("#area").val()) || 0;
      var avg_selling_price = parseFloat($("#avg_selling_price").val()) || 0;

      $("input[name=avg_selling_price]").each(function() {
        toplam = avg_selling_price / area;
      })
      $("input[name=selling_price_per_m2]").val(toplam);
    })
  });
  $(document).ready(function(e) {
    $("input").change(function() {
      var avg_rental_price_monthly = parseFloat($("#avg_rental_price_monthly").val()) || 0;

      value = avg_rental_price_monthly * 12;
      $("input[name=avg_rental_price_annually]").val(value);
    })
  });
  $(document).ready(function(e) {
    $("input").change(function() {
      var avg_rental_price_annually = parseFloat($("#avg_rental_price_annually").val()) || 0;
      value = 0.05 * avg_rental_price_annually;
      $("input[name=agent_fee_annually]").val(value);
    })
  });
  $(document).ready(function(e) {
    $("input").change(function() {
      var avg_rental_price_monthly = parseFloat($("#avg_rental_price_monthly").val()) || 0;
      var avg_selling_price = parseFloat($("#avg_selling_price").val()) || 0;
      value = avg_rental_price_monthly * 12;
      value_total = value / avg_selling_price;
      pecentage = value_total * 100;
      $("input[name=avg_rental_price_annually_percentage]").val(pecentage);
    })
  });
  $(document).ready(function(e) {
    $("input").change(function() {
      var avg_rental_price_annually = parseFloat($("#avg_rental_price_annually").val()) || 0;
      var agent_fee_annually = parseFloat($("#agent_fee_annually").val()) || 0;
      var service_charge_annually = parseFloat($("#service_charge_annually").val()) || 0;
      var avg_selling_price = parseFloat($("#avg_selling_price").val()) || 0;
      value = avg_rental_price_annually-agent_fee_annually-service_charge_annually;
      value_total = value / avg_selling_price;
      pecentage = value_total * 100;
      $("input[name=expected_annual_return]").val(pecentage);
    })
  });
</script>
  <script type="text/javascript">
    
    var rupiah = document.getElementById('avg_selling_price');
    rupiah.addEventListener('keyup', function(e){
      // tambahkan 'Rp.' pada saat form di ketik
      // gunakan fungsi formatRupiah() untuk mengubah angka yang di ketik menjadi format angka
      rupiah.value = formatRupiah(this.value);
    });
 
    /* Fungsi formatRupiah */
    function formatRupiah(angka, prefix){
      var number_string = angka.replace(/[^,\d]/g, '').toString(),
      split       = number_string.split(','),
      sisa        = split[0].length % 3,
      rupiah        = split[0].substr(0, sisa),
      ribuan        = split[0].substr(sisa).match(/\d{3}/gi);
 
      // tambahkan titik jika yang di input sudah menjadi angka ribuan
      if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
      }
 
      rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
      return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }
  </script>
</body>
</html>
